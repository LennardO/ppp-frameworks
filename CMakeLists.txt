cmake_minimum_required(VERSION 3.18)
project(PPP-Frameworks VERSION 0.1
                       DESCRIPTION "analysis of performance-portability frameworks")

set(CMAKE_CXX_STANDARD_REQUIRED 17)

set(CMAKE_VERBOSE_MAKEFILE ON)

#at least one target needs to be enabled
if(NOT USE_OPENMP AND
   NOT USE_CUDA AND
   NOT USE_KOKKOS AND
   NOT USE_RAJA AND
   NOT USE_SYCL AND
   NOT USE_OPENMP-TARGET)

  set(USE_CUDA ON)
endif()

#OpenMP is always needed as it is used on the host
find_package(OpenMP REQUIRED)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")

if(USE_OPENMP)
  add_compile_definitions(CF_USE_OPENMP)
endif()
if(USE_CUDA)
  add_compile_definitions(CF_USE_CUDA)
endif()
if(USE_OPENCL)
  find_package(OpenCL REQUIRED)
  add_compile_definitions(CF_USE_OPENCL)
endif()
if(USE_KOKKOS)
  find_package(Kokkos REQUIRED)
  add_compile_definitions(CF_USE_KOKKOS)
endif()
if(USE_RAJA)
  set(ENABLE_OPENMP ON)
  set(ENABLE_CUDA ON)
  find_package(Threads)
  set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} --expt-extended-lambda -U __GLIBCXX_TYPE_INT_N_0")

  set(BLT_CXX_STD c++${CMAKE_CXX_STANDARD_REQUIRED})
  include(blt-submodule/SetupBLT.cmake)

  find_package(RAJA CONFIG REQUIRED)
  blt_print_target_properties(TARGET RAJA)

  if(DEFAULT_GPU)
    set(raja-depends RAJA cuda)
    set(raja-defines POLICY_CUDA)
  else()
    set(raja-depends RAJA openmp)
    set(raja-defines POLICY_OPENMP)
  endif()
  add_compile_definitions(CF_USE_RAJA)
endif()
if(USE_SYCL)
  find_package(hipSYCL CONFIG REQUIRED)

  set(HIPSYCL_DEBUG_LEVEL 3)
  cmake_policy(SET CMP0005 NEW)
  add_compile_definitions(HIPSYCL_DEBUG_LEVEL=${HIPSYCL_DEBUG_LEVEL})

  add_compile_definitions(CF_USE_SYCL)
endif()
if(USE_OPENMP-TARGET)
  if(DEFAULT_GPU)
    add_compile_options(-mp=gpu)
  endif()

  add_compile_definitions(CF_USE_OPENMP_TARGET)
endif()

if(DEFAULT_GPU)
  add_compile_definitions(CF_DEFAULT_TARGET_GPU)
endif()

add_subdirectory(libs)
add_subdirectory(CaptainFuture)
add_subdirectory(unifiedPP)
add_subdirectory(examples)