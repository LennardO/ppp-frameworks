//
// Created by lennard on 4/30/21.
//

#pragma once

#include "Target.h"

namespace cf {

  enum MemorySpace {
    CPU,
    GPU
  };

  MemorySpace defaultMemorySpace(Target target) const {
    if (target == Target::CPU)
      return MemorySpace::CPU;
    return MemorySpace::GPU;
  }
}