//
// Created by lennard on 5/3/21.
//

#pragma once

#include "ArrayHandler.hpp"
#include "join.hpp"
#include "impl/ParallelForall.hpp"
#include "impl/ParallelReduction.hpp"
#include "impl/ParallelScan.hpp"

#include <mutex>


namespace cf {
  using NodeID = int;

  class Node {
  public:

    Node(Target target)
    : target_{target} {}

    [[nodiscard]] Target target() const {
      return target_;
    }

    template<typename T>
    void addReadDependency(ArrayHandler<T>& ah, DataLocation dataLocation) {
      reading_.emplace_back(&ah, dataLocation); //todo: geht schief wenn arrayHandler zuerst zerstört wird
    }

    template<typename T>
    void addWriteDependency(ArrayHandler<T>& ah, DataLocation dataLocation) {
      writing_.emplace_back(&ah, dataLocation);
    }

    template<typename T>
    void addReadWriteDependency(ArrayHandler<T>& ah, DataLocation dataLocation) {
      reading_.emplace_back(&ah, dataLocation);
      writing_.emplace_back(&ah, dataLocation);
    }

    [[nodiscard]] std::vector <ArrayHandlerID> readDependencies() const {
      std::vector <ArrayHandlerID> result;
      for (auto const& ah : reading_)
        result.emplace_back(ah.first->id());
      return result;
    }

    [[nodiscard]] std::vector <ArrayHandlerID> writeDependencies() const {
      std::vector <ArrayHandlerID> result;
      for (auto const& ah : writing_)
        result.emplace_back(ah.first->id());
      return result;
    }

    template<typename F>
    void forall(size_t N, F f) {
      parallelExecution_ = std::make_shared<ParallelForall<F>>(f, N);
    }

    template<typename T, typename IF, typename JF>
    void reduce(T& reductionVariable, IF inputFunction, JF joinFunction, size_t N) {
      parallelExecution_ = std::make_shared<ParallelReduction<T,IF,JF>>(reductionVariable, inputFunction, joinFunction, N);
    }

    template<typename T, typename JF>
    void scan(ArrayHandler<T>& out_ah, ArrayHandler<T>& in_ah, T initialValue, JF joinFunction) {
      auto in_dvc = in_ah.readView(*this);
      auto inputFunction = CF_LAMBDA(size_t i)->T { return in_dvc[i]; };
      scan(out_ah, inputFunction, initialValue, joinFunction);
    }

    template<typename T, typename IF, typename JF>
    void scan(ArrayHandler<T>& out_ah, IF inputFunction, T initialValue, JF joinFunction) {
      auto out_dvc = out_ah.readWriteView(*this);
      parallelExecution_ = std::make_shared<ParallelScan<T,IF,JF>>(out_dvc,
                                                                   inputFunction,
                                                                   initialValue,
                                                                   joinFunction,
                                                                   out_ah.size());
    }

    void transferData() const {

      for (auto[ah_ptr, dataLocation] : reading_)
        ah_ptr->transferTo(dataLocation);
      for (auto[ah_ptr, dataLocation] : writing_) {
        ah_ptr->transferTo(dataLocation);
        ah_ptr->setCurrentDataLocation(dataLocation);
      }
    }

    void execute() const {
      if (target_ == GPU) {
        std::lock_guard<std::mutex> guard(cudaMutex);
        transferData();
        parallelExecution_->runCuda();
      }
      else if (target_ == CPU) {
        std::lock_guard<std::mutex> guard(ompMutex);
        transferData();
        parallelExecution_->runOpenMP();
      }
      else
        throw std::runtime_error("unsupported target ");
    }

  private:
    std::shared_ptr <ParallelExecutionInterface> parallelExecution_ = nullptr;
    Target target_;
    std::vector <std::pair<ArrayHandlerInterface*, DataLocation>> reading_;
    std::vector <std::pair<ArrayHandlerInterface*, DataLocation>> writing_;
    std::mutex cudaMutex_;
    std::mutex ompMutex_;
  };

}