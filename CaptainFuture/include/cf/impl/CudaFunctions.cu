//
// Created by lennard on 4/4/21.
//

#pragma once

#include <iostream>

#ifndef __CUDACC_EXTENDED_LAMBDA__
  #error "please compile with --expt-extended-lambda"
#endif

namespace cf {

  #define checkError(ans) { cudaAssert((ans), __FILE__, __LINE__); }
  inline void cudaAssert(cudaError_t code, const char *file, int line)
  {
    if (code != cudaSuccess)
      throw std::runtime_error(std::string("cuda error: ") + cudaGetErrorString(code)
                               + " in " + file + " at " + std::to_string(line) + "\n");
  }

  template<typename T>
  T* cudaAllocate(int N) {
    T* device_ptr;
    checkError(cudaMalloc(&device_ptr, sizeof(T) * N));
    return device_ptr;
  }

  template<typename T>
  void cudaCopyToHost(T* host_ptr, const T* device_ptr, int N) {
    checkError(cudaMemcpy(host_ptr, device_ptr, sizeof(T) * N, cudaMemcpyDeviceToHost));
  }

  template<typename T>
  void cudaCopyToDevice(T* device_ptr, const T* host_ptr, int N) {
    checkError(cudaMemcpy(device_ptr, host_ptr, sizeof(T) * N, cudaMemcpyHostToDevice));
  }

  template<typename T>
  void cudaCopyInsideDevice(T* sink_ptr, const T* source_ptr, int N) {
    checkError(cudaMemcpy(sink_ptr, source_ptr, sizeof(T) * N, cudaMemcpyDeviceToDevice));
  }

  template<typename T>
  void cudaDeviceFree(T* device_ptr) {
    checkError(cudaFree(device_ptr));
  }

  template<class F>
  __global__
  void cudaForallKernel(F f, int N){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i<N)
      f(i);
  }

  template <unsigned int blockSize, typename T, typename JF>
  __device__ void warpReduce(volatile T* sdata, unsigned int tid, JF joinFunction) {
    //if statements are evaluated at compile time
    if(blockSize >= 64) sdata[tid] = joinFunction(sdata[tid], sdata[tid + 32]);
    if(blockSize >= 32) sdata[tid] = joinFunction(sdata[tid], sdata[tid + 16]);
    if(blockSize >= 16) sdata[tid] = joinFunction(sdata[tid], sdata[tid +  8]);
    if(blockSize >=  8) sdata[tid] = joinFunction(sdata[tid], sdata[tid +  4]);
    if(blockSize >=  4) sdata[tid] = joinFunction(sdata[tid], sdata[tid +  2]);
    if(blockSize >=  2) sdata[tid] = joinFunction(sdata[tid], sdata[tid +  1]);
  }

  template <unsigned int blockSize, typename T, typename IF, typename JF>
  __global__ void reduce(IF inputFunction, JF joinFunction, T neutralElement, T* blockResults_dvc, unsigned int N) {
    extern __shared__ T sdata[];
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*(blockSize*2) + tid;
    unsigned int gridSize = blockSize*2*gridDim.x;
    sdata[tid] = neutralElement;
    while (i < N) {
      sdata[tid] = joinFunction(sdata[tid], joinFunction(inputFunction(i), inputFunction(i+blockSize)));
      i += gridSize;
    }
    __syncthreads();

    //leftmost if statements are evaluated at compile time
    if(blockSize >= 512) { if(tid < 256) { sdata[tid] = joinFunction(sdata[tid], sdata[tid+256]); } __syncthreads(); }
    if(blockSize >= 256) { if(tid < 128) { sdata[tid] = joinFunction(sdata[tid], sdata[tid+128]); } __syncthreads(); }
    if(blockSize >= 128) { if(tid <  64) { sdata[tid] = joinFunction(sdata[tid], sdata[tid+ 64]); } __syncthreads(); }

    if(tid < 32) warpReduce<blockSize,T,JF>(sdata, tid, joinFunction);
    if(tid == 0) blockResults_dvc[blockIdx.x] = sdata[0];
  }

  template<typename T, typename IF, typename JF>
  __global__ void scan_stage1(T       neutralElement,
                              T*      out_dvc,
                              IF      inputFunction,
                              T*      threadResults_dvc,
                              JF      joinFunction,
                              size_t  numThreads,
                              size_t  workSize,
                              size_t  size) {
    size_t tid = blockIdx.x*blockDim.x + threadIdx.x;
    if(tid >= numThreads)
      return;
    size_t start = tid*workSize;
    if( start+workSize > size ) workSize = size - start;

    T result = neutralElement;
    for(size_t i=start; i<start+workSize; i++) {
      result = joinFunction(result, inputFunction(i));
      out_dvc[i] = result;
    }
    threadResults_dvc[tid] = result;
  }

  template<typename T, typename JF>
  __global__ void scan_stage2(T         neutralElement,
                              T*        out_dvc,
                              const T*  threadResults_dvc,
                              JF        joinFunction,
                              size_t    numThreads,
                              size_t    workSize,
                              size_t    size) {
    size_t tid = blockIdx.x*blockDim.x + threadIdx.x;
    if(tid >= numThreads)
      return;
    size_t start = tid*workSize;
    if( start+workSize > size ) workSize = size - start;

    T offset = neutralElement;
    for(size_t i=0; i<tid; i++)
      offset = joinFunction(offset, threadResults_dvc[i]);

    for(size_t i=start; i<start+workSize; i++)
      out_dvc[i] = joinFunction(out_dvc[i], offset);
  }

}