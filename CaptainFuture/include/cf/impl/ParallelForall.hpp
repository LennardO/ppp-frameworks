//
// Created by lennard on 4/4/21.
//

#pragma once

#include "ParallelExecutionInterface.hpp"
#include "CudaFunctions.cu"

namespace cf {

  template<typename F>
  class ParallelForall : public ParallelExecutionInterface {
  public:
    ParallelForall(F f, size_t size)
    : lambda_{f}
    , size_{size}
    {}

    void runCuda() override {
      auto t0 = std::chrono::steady_clock::now();

      size_t blockSize = 256;
      size_t numBlocks = (size_ + blockSize - 1) / blockSize; // aufrunden falls N kein Mehrfaches von blockSize ist

      cudaForallKernel<<<numBlocks, blockSize>>>(lambda_, size_);
      checkError(cudaPeekAtLastError());

      auto t1 = std::chrono::steady_clock::now();
      std::cout << "executing forall with Cuda:"
                << " numBlocks=" << numBlocks
                << " blockSize=" << blockSize << " took " << t1-t0 << '\n';
    }

    void runOpenMP() override {
      auto t0 = std::chrono::steady_clock::now();

      #pragma omp parallel for
      for (size_t i = 0; i < size_; i++)
        lambda_(i);

      auto t1 = std::chrono::steady_clock::now();
      std::cout << "executing forall with OpenMP took " << t1-t0 << '\n';
    }

  private:
    F lambda_;
    size_t size_;
  };
}