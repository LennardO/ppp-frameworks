//
// Created by lennard on 4/4/21.
//

#pragma once

#include "ParallelExecutionInterface.hpp"
#include "CudaFunctions.cu"
#include <omp.h>

namespace cf {

  template<typename T, typename IF, typename JF, unsigned int blockSize=256>
  class ParallelReduction : public ParallelExecutionInterface {
  public:
    ParallelReduction(T& reductionVariable, IF inputFunction, JF joinFunction, size_t size)
    : reductionVariable_{reductionVariable}
    , neutralElement_{reductionVariable}
    , inputFunction_{inputFunction}
    , joinFunction_{joinFunction}
    , size_{size}
    {
      blockResults_.resize(numBlocks_);
    }

    ~ParallelReduction() {
      if(blockResults_dvc_ != nullptr)
        cudaDeviceFree(blockResults_dvc_);
    }

    void runCuda() override {
      std::cout << "start of cuda reduction >" << cf::elapsedTime() << '\n';
      auto t0 = std::chrono::steady_clock::now();
      blockResults_dvc_ = cudaAllocate<T>(numBlocks_);
      std::cout << "blockResults allocated >" << cf::elapsedTime() << '\n';

      reductionVariable_ = neutralElement_;
      reduce<blockSize, T, IF, JF><<<numBlocks_, blockSize, blockSize*sizeof(T)>>>(inputFunction_,
                                                                                   joinFunction_,
                                                                                   neutralElement_,
                                                                                   blockResults_dvc_,
                                                                                   size_);
      checkError(cudaPeekAtLastError());
      checkError(cudaDeviceSynchronize());

      cudaCopyToHost<T>(blockResults_.data(), blockResults_dvc_, numBlocks_);
      for(int i=0; i<numBlocks_; i++)
        reductionVariable_ = joinFunction_(reductionVariable_, blockResults_[i]);

      auto t1 = std::chrono::steady_clock::now();
      std::cout << "executing reduction with Cuda: numBlocks=" << numBlocks_
                << " blockSize=" << blockSize << " took " << t1-t0 << " >" << elapsedTime() << '\n';
      std::cout << "...note: using " << neutralElement_ << " as the neutral element\n";
    }

    void runOpenMP() override {
      auto t0 = std::chrono::steady_clock::now();

      int nThreads;
      #pragma omp parallel
      {
        #pragma omp single
        nThreads = omp_get_num_threads();
        T localVar = reductionVariable_;
        #pragma omp for
        for(size_t i = 0; i < size_; i++)
          localVar = joinFunction_(localVar, inputFunction_(i));

        #pragma omp critical
        reductionVariable_ = joinFunction_(reductionVariable_, localVar);
      }

      auto t1 = std::chrono::steady_clock::now();
      std::cout << "executing reduction with OpenMP using " << nThreads << " threads took " << t1-t0 << " >" << elapsedTime() << '\n';
      std::cout << "...note: using " << neutralElement_ << " as the neutral element" << std::endl;
    }

  private:
    T& reductionVariable_;
    T neutralElement_;
    IF inputFunction_;
    JF joinFunction_;
    size_t size_;
    T* blockResults_dvc_ = nullptr;
    std::vector<T> blockResults_;
    const size_t numBlocks_ = 32;
  };
}