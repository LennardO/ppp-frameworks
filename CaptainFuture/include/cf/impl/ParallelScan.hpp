//
// Created by lennard on 4/4/21.
//

#pragma once

#include "ParallelExecutionInterface.hpp"
#include "CudaFunctions.cu"

namespace cf {

  template<typename T, typename IF, typename JF>
  class ParallelScan : public ParallelExecutionInterface {
  public:
    ParallelScan(T* out_dvc, IF inputFunction, T neutralElement, JF joinFunction, size_t size)
    : out_dvc_{out_dvc}
    , inputFunction_{inputFunction}
    , neutralElement_{neutralElement}
    , joinFunction_{joinFunction}
    , size_{size}
    {}

    void runCuda() override {
      auto t0 = std::chrono::steady_clock::now();

      const size_t workSize = 8192;
      size_t blockSize = 256;
      size_t numThreads = (size_+workSize-1) / workSize;
      size_t numBlocks = (numThreads+blockSize-1) / blockSize;

      T* threadResults_dvc = cudaAllocate<T>(numThreads);

      scan_stage1<T,IF,JF><<<numBlocks,blockSize>>>(neutralElement_,
                                                    out_dvc_,
                                                    inputFunction_,
                                                    threadResults_dvc,
                                                    joinFunction_,
                                                    numThreads,
                                                    workSize,
                                                    size_);
      checkError(cudaPeekAtLastError());
      scan_stage2<T,JF><<<numBlocks,blockSize>>>(neutralElement_,
                                                 out_dvc_,
                                                 threadResults_dvc,
                                                 joinFunction_,
                                                 numThreads,
                                                 workSize,
                                                 size_);
      checkError(cudaPeekAtLastError());

      cudaDeviceFree(threadResults_dvc);

      auto t1 = std::chrono::steady_clock::now();
      std::cout << "executing scan with Cuda:"
                << " workSize=" << workSize
                << " numThreads=" << numThreads
                << " numBlocks=" << numBlocks
                << " blockSize=" << blockSize << " took " << t1-t0 << " >" << elapsedTime() << '\n';
      std::cout << "...note: using " << neutralElement_ << " as the neutral element\n";
    }

    void runOpenMP() override {
      auto t0 = std::chrono::steady_clock::now();

      T* localResults;
      int nThreads;
      #pragma omp parallel
      {
        int threadNum = omp_get_thread_num();
        #pragma omp single
        {
          nThreads = omp_get_num_threads();
          localResults = (T*) malloc(sizeof(T) * (nThreads + 1));
          localResults[0] = neutralElement_;
        }

        T localResult = neutralElement_;
        #pragma omp for schedule(static) nowait
        for (int i=0; i<size_; i++) {
          localResult = joinFunction_(localResult, inputFunction_(i));
          out_dvc_[i] = localResult;
        }
        localResults[threadNum+1] = localResult;

        #pragma omp barrier

        T offset = neutralElement_;
        for(int i=0; i<(threadNum+1); i++)
          offset = joinFunction_(offset, localResults[i]);

        #pragma omp for schedule(static)
        for (int i=0; i<size_; i++)
          out_dvc_[i] = joinFunction_(out_dvc_[i], offset);
      }
      free(localResults);

      auto t1 = std::chrono::steady_clock::now();
      std::cout << "executing scan with OpenMP using " << nThreads << " threads took " << t1-t0 << " >" << elapsedTime() << '\n';
    }

  private:
    size_t size_;
    T* out_dvc_;
    IF inputFunction_;
    JF joinFunction_;
    T neutralElement_;

  };
}