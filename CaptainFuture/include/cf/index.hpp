#pragma once

#include <array>

namespace cf {
  template<int D=1>
  class index {
  public:
    index()
    : values_{}
    {}

    template<int dimensions = D, typename = std::enable_if_t<dimensions == 1>>
    index(size_t dim0)
    : values_{dim0}
    {}
    template<int dimensions = D, typename = std::enable_if_t<dimensions == 2>>
    index(size_t dim0, size_t dim1)
    : values_{dim0, dim1}
    {}
    template<int dimensions = D, typename = std::enable_if_t<dimensions == 3>>
    index(size_t dim0, size_t dim1, size_t dim2)
    : values_{dim0, dim1, dim2}
    {}

    template<int dimensions = D, typename = std::enable_if_t<dimensions == 1>>
    operator size_t() const {
      return values_[0];
    }

    size_t operator[](int d) {
      return values_[d];
    }

  private:
    std::array<size_t, D> values_;
  };
}