//
// Created by lennard on 5/9/21.
//

#pragma once

namespace cf {

  template<typename T>
  class Add {
  public:
    __host__ __device__
    T operator()(T left, T right) {
      return left + right;
    }
    constexpr static T neutralElement() {
      return (T) 0;
    }
  };

  template<typename T>
  class Mul {
  public:
    __host__ __device__
    T operator()(T left, T right) {
      return left * right;
    }
  };

  template<typename T>
  class Min {
  public:
    __host__ __device__
    T operator()(T left, T right) {
      return left < right ? left : right;
    }
  };

  template<typename T>
  class Max {
  public:
    __host__ __device__
    T operator()(T left, T right) {
      return left > right ? left : right;
    }
  };

  template<typename T>
  class BitwiseAnd {
  public:
    __host__ __device__
    T operator()(T left, T right) {
      return left & right;
    }
  };

  template<typename T>
  class BitwiseOR {
  public:
    __host__ __device__
    T operator()(T left, T right) {
      return left | right;
    }
  };

}