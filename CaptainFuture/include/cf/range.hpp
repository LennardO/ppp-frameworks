//
// Created by lennard on 7/27/21.
//

#pragma once

#include "index.hpp"
#include <array>

namespace cf {
  template<int D=1>
  class range {
  public:
    range()
    : start_{}
    , end_{}
    {}

    range(index<D> end)
    : start_{}
    , end_{end}
    {}
    range(index<D> start, index<D> end)
    : start_{start}
    , end_{end}
    {}

    size_t size() const {
      size_t result=1;
      for(int i=0; i<D; i++)
        result *= size(i);
      return result;
    }

    size_t size(int dim) const {
      return end_[dim] - start_[dim];
    }


  private:
    index<D> start_;
    index<D> end_;
  };
}