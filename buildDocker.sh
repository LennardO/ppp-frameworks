#!/usr/bin/env bash
set -e

docker build . -f dockerfiles/Cuda -t cuda
docker build . -f dockerfiles/Kokkos -t kokkos
docker build . -f dockerfiles/Raja -t raja
docker build . -f dockerfiles/HipSYCL -t sycl
docker build . -f dockerfiles/OpenMP-Target -t openmp-target