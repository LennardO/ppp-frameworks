FROM ubuntu:focal

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
		apt-get install -y \
			software-properties-common \
			apt-transport-https \
			ca-certificates \
			gnupg \
			apt-utils \
			wget \
			curl \
			git \
			gcc-10 \
			g++-10 \
			gdb

RUN wget -qO - https://apt.kitware.com/keys/kitware-archive-latest.asc | apt-key add - && \
		apt-add-repository 'deb https://apt.kitware.com/ubuntu/ focal main' && \
		apt-get update && \
		apt-get install -y cmake

RUN apt-get install -y \
			libilmbase-dev \
			libglew-dev \
			libglfw3-dev

RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin && \
		mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600 && \
		wget https://developer.download.nvidia.com/compute/cuda/11.2.1/local_installers/cuda-repo-ubuntu2004-11-2-local_11.2.1-460.32.03-1_amd64.deb && \
		dpkg -i cuda-repo-ubuntu2004-11-2-local_11.2.1-460.32.03-1_amd64.deb && \
		apt-key add /var/cuda-repo-ubuntu2004-11-2-local/7fa2af80.pub && \
		rm cuda-repo-ubuntu2004-11-2-local_11.2.1-460.32.03-1_amd64.deb && \
		apt-get update && apt-get install -y cuda
#for cuda-gdb:
RUN apt-get install -y libtinfo5 libncursesw5

ENV PATH=/usr/local/cuda/bin${PATH:+:${PATH}}
ENV CUDACXX=/usr/local/cuda/bin/nvcc

# install specific llvm version:
RUN wget https://apt.llvm.org/llvm.sh && \
		chmod +x llvm.sh && \
		./llvm.sh 11
# darf erst nach der cuda installation gesetzt werden,
# da sonst nvidia-dkms-455 nicht installiert werden kann
# https://askubuntu.com/questions/1211167/cant-install-nvidia-drivers-on-ubuntu-19-10-after-kernel-update
RUN update-alternatives --install /usr/bin/cc cc /usr/bin/clang-11 110 && \
		update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-11 110

RUN apt-get clean
