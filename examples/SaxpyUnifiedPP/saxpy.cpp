
#include "saxpy.h"
#include <unifiedPP.hpp>
#include <iostream>
#include <chrono>

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {

  uPP::range N = x.size();

  auto t0 = std::chrono::steady_clock::now();
  uPP::ArrayHandler<float> x_ah(x);
  std::cout << "AH " << x_ah.id() << ": allocation took " << std::chrono::steady_clock::now() - t0 << '\n';
  t0 = std::chrono::steady_clock::now();
  uPP::ArrayHandler<float> y_ah(y);
  std::cout << "AH " << y_ah.id() << ": allocation took " << std::chrono::steady_clock::now() - t0 << '\n';

  auto x_dvc = x_ah.readView();
  auto y_dvc = y_ah.readWriteView();

  uPP::forall(N, UPP_LAMBDA(uPP::index i) {
    y_dvc[i] = a * x_dvc[i] + y_dvc[i];
  });
}
