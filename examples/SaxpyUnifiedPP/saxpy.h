#pragma once
#include <vector>

// “Single-Precision A·X Plus Y”
// computes y = alpha * x + y
void saxpy(float a, const std::vector<float>& x, std::vector<float>& y);
