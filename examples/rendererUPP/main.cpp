//
// Created by lennard on 3/10/21.
//

#include "OctreeGridSearch.h"
#include <OpenglWindow.h>

int main() {
  uPP::Context ctx;

  std::string gridFile = "data/models/AhmedBody/AhmedBody_2_1MioCells_Var_p_cropped_ranked_header_LinkedMini-2.bin";
  u_long maxRegionSize = 512;

  gb::Grid grid;
  grid.loadFromFile(gridFile);

  OctreeGridSearch ogs(ctx);
  auto t0 = std::chrono::steady_clock::now();
  ogs.construct(grid, maxRegionSize);
  auto t1 = std::chrono::steady_clock::now();
  std::cout << "construction time: " << t1-t0 << std::endl;

  OpenglWindow window;
  window.create();

  OpenglBuffer glBuffer(window.width(), window.height());
  glBuffer.create();
  ogs.setOutputBuffer(glBuffer);

  gb::Camera camera;
  camera.setAspectRatio(window.aspectRatio());
  camera.targetFullFrame(grid.aabb());

  t0 = std::chrono::steady_clock::now();
  ogs.render(camera);
  t1 = std::chrono::steady_clock::now();
  std::cout << "frame rendering time: " << t1-t0 << std::endl;

  glBuffer.drawToFrame();
  window.finalizeFrame();

  std::cin.get();

  window.close();
  return 0;
}