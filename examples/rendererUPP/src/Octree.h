//
// Created by lennard on 3/10/21.
//

#pragma once

#include <geometry/AABox.h>
#include <unifiedPP.hpp>

using OctreeID = u_short;
const OctreeID INVALID_OCTREE_ID = std::numeric_limits<OctreeID>::max();

class Octree {
public:
  gb::AABox aaBox;
  gb::PrimitiveID regionStart = gb::INVALID_PRIMITIVE_ID;
  gb::RegionSizeT regionSize = 0;
  OctreeID leftChild = INVALID_OCTREE_ID;
  OctreeID rightSibling = INVALID_OCTREE_ID;
  OctreeID parent = INVALID_OCTREE_ID;

  [[nodiscard]] bool isLeaf() const;
};

using OctreeView = uPP::Array<Octree>::View;
using ConstOctreeView = uPP::Array<Octree>::ConstView;

template <typename Functor>
ENABLE_DEVICE
void traverse(ConstOctreeView octreeNodes, const gb::Ray& ray, float& dataSum, float sumMax, Functor lambda) {
  OctreeID nodeID = 0;
  bool goingUp = false;
  while(nodeID != INVALID_OCTREE_ID && dataSum < sumMax) {
    auto node = octreeNodes[nodeID];
    if(goingUp) {
      if (node.rightSibling != INVALID_OCTREE_ID) {
        goingUp = false;
        nodeID = node.rightSibling;
      }
      else
        nodeID = node.parent;
      continue;
    }

    bool intersected = ray.intersects(node.aaBox);
    bool isLeaf = node.regionStart != gb::INVALID_PRIMITIVE_ID;

    if(isLeaf && intersected) {
      lambda(node);
    }

    if( !isLeaf && intersected ) {
      nodeID = node.leftChild;
      continue;
    }

    bool hasRightSibling = node.rightSibling != INVALID_OCTREE_ID;
    nodeID = hasRightSibling ? node.rightSibling : node.parent;
    goingUp = !hasRightSibling;
  }
}
//  void traverse(const Octree* octreeNodes, const Ray& ray, float& dataSum, float sumMax, std::function<void(Octree)>& lambda);
