
#include "testAsyncNodes.h"
#include <cf/asyncNodes.hpp>

double testAsyncNodes(double a, const std::vector<double>& x, const std::vector<double>& y) {

// x----1
//     / \
// y--0   3--s
//     \ /
//      2

// s = <x+ay, scan(ay,+)>
// 0: h = a*y
// 1: t = x+h
// 2: k = scan(h,+)
// 3: s = <t, k>

  size_t N = y.size();
  size_t xN = x.size();

  cf::ArrayHandler<double> x_ah(x);
  cf::ArrayHandler<double> y_ah(y);
  cf::ArrayHandler<double> t_ah(xN);
  cf::ArrayHandler<double> k_ah(N);

  using namespace std::chrono_literals;
  auto node1 = cf::createNode([&](cf::Target target) {
    auto x_dvc = x_ah.readView(target);
    auto t_dvc = t_ah.writeView(target);
    cf::forall(N, target, CF_LAMBDA(size_t i) {
      t_dvc[i] = a * x_dvc[i];
    });
    cf::scan(t_ah, t_ah, target, (double)0, cf::Add<double>{});
//    cf::scan(t_ah, x_ah, target, (double)0, cf::Add<double>{});
  });
  double rT = 0;
  auto node2 = cf::createNode([&](cf::Target target) {
    auto t_dvc = t_ah.readView(target);
    cf::reduce(rT, CF_LAMBDA(size_t i)->double { return t_dvc[i]; }, cf::Add<double>{}, xN, target);
  });
  auto node3 = cf::createNode([&](cf::Target target) {
    cf::scan(k_ah, y_ah, target, (double)0, cf::Add<double>{});
  });
  double rY = 0;
  auto node4 = cf::createNode([&](cf::Target target) {
    auto k_dvc = k_ah.readView(target);
    cf::reduce(rY, CF_LAMBDA(size_t i)->double { return k_dvc[i]; }, cf::Add<double>{}, N, target);
  });
  double result;
  auto node5 = cf::createNode([&](cf::Target target) {
    result = rT+rY;
  });
  node5(cf::CPU,
        {
          node2(cf::CPU, {
                  node1(cf::CPU)
                }),
          node4(cf::GPU, {
            node3(cf::GPU)
          })
        }).wait();

//  size_t N = y.size();
//  size_t xN = x.size();
//
//  cf::ArrayHandler<double> x_ah(x);
//  cf::ArrayHandler<double> y_ah(y);
//  cf::ArrayHandler<double> t_ah(xN);
//  cf::ArrayHandler<double> k_ah(N);
//
//  auto node0 = cf::createNode([&](cf::Target target) {
//    cf::scan(t_ah, x_ah, target, (double)0, cf::Add<double>{});
//  });
//
//  double rT = 0;
//  auto node1 = cf::createNode([&](cf::Target target) {
//    auto t_dvc = t_ah.readView(target);
//    std::cout << "inside node1 >" << cf::elapsedTime() << '\n';
//    cf::reduce(rT, CF_LAMBDA(size_t i)->double { return t_dvc[i]; }, cf::Add<double>{}, xN, target);
//    std::cout << "end of node1 >" << cf::elapsedTime() << '\n';
//  });
//
//  auto node2 = cf::createNode([&](cf::Target target) {
//    cf::scan(k_ah, y_ah, target, (double)0, cf::Add<double>{});
//  });
//
//  double rY = 0;
//  auto node3 = cf::createNode([&](cf::Target target) {
//    auto k_dvc = k_ah.readView(target);
//    cf::reduce(rY, CF_LAMBDA(size_t i)->double { return k_dvc[i]; }, cf::Add<double>{}, N, target);
//  });
//
//  double result;
//  auto node4 = cf::createNode([&](cf::Target target) {
//    result = rT+rY;
//  });
//
//  auto node0_ftr = node0(cf::GPU);
//  auto node1_ftr = node1(cf::GPU, { node0_ftr });
//  auto node2_ftr = node2(cf::CPU);
//  auto node3_ftr = node3(cf::CPU, { node2_ftr });
//  auto node4_ftr = node4(cf::CPU, { node1_ftr, node3_ftr });
//
//  node4_ftr.wait();

//  node4(cf::CPU,
//        {
//          node1(cf::GPU, {
//                  node0(cf::GPU)
//                }),
//          node3(cf::CPU, {
//            node2(cf::CPU)
//          })
//        }).wait();
  std::cout << std::endl;
  return result;
//
//  cf::ArrayHandler<double> x_ah(x);
//  cf::ArrayHandler<double> y_ah(y);
//  cf::ArrayHandler<double> h_ah(N);
//  cf::ArrayHandler<double> t_ah(N);
//  cf::ArrayHandler<double> k_ah(N);
//
//  auto node0 = cf::createNode([&](cf::Target target) {
//    auto y_dvc = y_ah.readView(target);
//    auto h_dvc = h_ah.writeView(target);
//
//    cf::forall(N, target, CF_LAMBDA(size_t i) {
//      h_dvc[i] = a * y_dvc[i];
//    });
//  });
//
//  auto node1 = cf::createNode([&](cf::Target target) {
//    auto x_dvc = x_ah.readView(target);
//    auto h_dvc = h_ah.readView(target);
//    auto t_dvc = t_ah.writeView(target);
//
//    cf::forall(N, target, CF_LAMBDA(size_t i) {
//      t_dvc[i] = x_dvc[i] + h_dvc[i];
//    });
//  });
//
//  auto node2 = cf::createNode([&](cf::Target target) {
//    cf::scan(k_ah, h_ah, target, (double)0, cf::Add<double>{});
//  });
//
//  double result = 0;
//  auto node3 = cf::createNode([&](cf::Target target) {
//    auto t_dvc = t_ah.readView(target);
//    auto k_dvc = k_ah.readView(target);
//
//    auto inputFunction = CF_LAMBDA(size_t i)->double { return t_dvc[i] * k_dvc[i]; };
//    cf::reduce(result, inputFunction, cf::Add<double>{}, N, target);
//  });
//
//
//  auto node0_ftr = node0(cf::GPU);
//
//  node3(cf::GPU, {
//          node1(cf::GPU, {
//            node0_ftr
//          }),
//          node2(cf::GPU, {
//            node0_ftr
//          })
//        }).wait();
//
//  return result;
}

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {
  size_t N = x.size();

  cf::ArrayHandler<float> x_ah(x);
  cf::ArrayHandler<float> y_ah(y);

  auto node0 = cf::createNode([&](cf::Target target) {
    auto x_dvc = x_ah.readView(target);
    auto y_dvc = y_ah.readWriteView(target);

    cf::forall(N, target, CF_LAMBDA(size_t i) {
      y_dvc[i] = a * x_dvc[i] + y_dvc[i];
    });
  });

  node0(cf::GPU).wait();
}