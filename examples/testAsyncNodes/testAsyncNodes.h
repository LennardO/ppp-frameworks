#pragma once
#include <vector>

// computes s = <x+ay, scan(ay,+)>
double testAsyncNodes(double a, const std::vector<double>& x, const std::vector<double>& y) ;

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y);