#pragma once
#include <vector>

// computes s = <x+ay, scan(ay,+)>
double testPipeline(double a, const std::vector<double>& x, const std::vector<double>& y) ;
