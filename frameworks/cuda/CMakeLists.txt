add_library(gridsearch
        src/OctreeGridSearch.cu
        src/CudaAABox.cu
        src/CudaPoint.cu
        src/CudaVector.cu src/CudaCamera.cu src/CudaRay.cu)
target_include_directories(gridsearch PUBLIC include)
target_compile_options(gridsearch PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
target_compile_options(gridsearch PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-lineinfo>)#todo: only in debug?
#target_compile_options(gridsearch PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:--expt-relaxed-constexpr)
target_compile_features(gridsearch PRIVATE cxx_std_17)
set_target_properties(
        gridsearch
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)
target_link_libraries(gridsearch PUBLIC gridbox-lib gridbox-window)


add_library(unified-ol
                include/UnifiedOffload.hpp
                src/DummyFile.cpp)
target_include_directories(unified-ol PUBLIC include)
target_compile_options(unified-ol PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
target_compile_options(unified-ol PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-lineinfo>)#todo: only in debug?
target_compile_features(unified-ol PRIVATE cxx_std_17)
set_target_properties(
        unified-ol
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)