//
// Created by lennard on 10/27/20.
//

#pragma once

#include "CudaPoint.h"
#include <geometry/AABox.h>
#include <array>

using namespace gridbox; //todo

class CudaAABox {
public:
  CudaPoint min, max;

  CudaAABox() = default;
  __device__ __host__
  CudaAABox(const CudaPoint& minPoint, const CudaPoint& maxPoint)
  : min(minPoint)
  , max(maxPoint)
  {};
  __device__ __host__
  CudaAABox(const AABox& other)
  : min(other._min)
  , max(other._max)
  {};

  CudaPoint mid() const;
  
  std::array<CudaAABox,8> octants() const {
    auto _mid = mid();

    CudaPoint wmBack  = {min[0],_mid[1],min[2]};
    CudaPoint mnMid   = {_mid[0],max[1],_mid[2]};
    CudaPoint wsMid   = {min[0],min[1],_mid[2]};
    CudaPoint mmFront = {_mid[0],_mid[1],max[2]};
    CudaPoint wmMid   = {min[0],_mid[1],_mid[2]};
    CudaPoint mnFront = {_mid[0],max[1],max[2]};
    CudaPoint msBack  = {_mid[0],min[1],min[2]};
    CudaPoint emMid   = {max[0],_mid[1],_mid[2]};
    CudaPoint msMid   = {_mid[0],min[1],_mid[2]};
    CudaPoint emFront = {max[0],_mid[1],max[2]};
    CudaPoint mmBack  = {_mid[0],_mid[1],min[2]};
    CudaPoint enMid   = {max[0],max[1],_mid[2]};

    return {{
              {min,    _mid},
              {wsMid,  mmFront},
              {wmBack, mnMid},
              {wmMid,  mnFront},
              {msBack, emMid},
              {msMid,  emFront},
              {mmBack, enMid},
              {_mid,    max}
            }};
  }

  __host__ __device__
  bool containsInInterior(const CudaPoint point) const;
};
