//
// Created by lennard on 10/27/20.
//

#pragma once

#include <algorithms/Octree.h>
#include "CudaAABox.h"

class CudaOctree {
public:
  CudaAABox aaBox;
  PrimitiveID regionStart = INVALID_PRIMITIVE_ID;
  RegionSizeT regionSize = 0;
  OctreeID leftChild = INVALID_OCTREE_ID;
  OctreeID rightSibling = INVALID_OCTREE_ID;
  OctreeID parent = INVALID_OCTREE_ID;

  __device__
  bool isLeaf() const {
    return regionStart != INVALID_PRIMITIVE_ID;
  }
};