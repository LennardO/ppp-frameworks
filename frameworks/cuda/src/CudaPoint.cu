//
// Created by lennard on 10/28/20.
//

#include "CudaPoint.h"

CudaPoint CudaPoint::halfWayTo(const CudaPoint other) const {
  return *this + 0.5 * (other - *this);
}

//inline bool operator==(const CudaPoint& lhs, const CudaPoint& rhs);
CudaPoint operator+(const CudaPoint lhs, const CudaVector rhs) {
  return {lhs.x()+rhs.x(), lhs.y()+rhs.y(), lhs.z()+rhs.z()};
}
CudaVector operator-(const CudaPoint lhs, const CudaPoint rhs) {
  return {lhs.x()-rhs.x(), lhs.y()-rhs.y(), lhs.z()-rhs.z()};
}