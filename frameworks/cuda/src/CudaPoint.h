//
// Created by lennard on 10/28/20.
//

#pragma once

#include "CudaVector.h"

class CudaPoint : public CudaTriple {
public:
  using CudaTriple::CudaTriple;

  CudaPoint halfWayTo(const CudaPoint other) const;

  using CudaTriple::operator[];
};

inline bool operator==(const CudaPoint lhs, const CudaPoint rhs);
__host__ __device__
CudaPoint operator+(const CudaPoint lhs, const CudaVector rhs);
__host__ __device__
CudaVector operator-(const CudaPoint lhs, const CudaPoint rhs);