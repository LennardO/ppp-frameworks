//
// Created by lennard on 11/2/20.
//

#include "CudaRay.h"

CudaRay::CudaRay(CudaPoint orgn, CudaVector drctn)
: origin(orgn)
, direction(drctn)
{
  mulInvDirection = {1.0f/direction.x(), 1.0f/direction.y(), 1.0f/direction.z()};
}

bool CudaRay::intersects(const CudaAABox aaBox) const {
  //todo
//  if(aaBox.containsInInterior(origin))
//    return true;

  const auto& min = aaBox.min;
  const auto& max = aaBox.max;

 float t[3];
  for (int i = 0; i < 3; i++) {
    if (direction.values[i] > 0)
      t[i] = (min[i] - origin[i]) * mulInvDirection[i];
    else if (direction[i] < 0)
      t[i] = (max[i] - origin[i]) * mulInvDirection[i];
    else
      t[i] = -INFINITY;
  }
  size_t maxIndex = 0;
  float maxT = t[0];
  if(t[1] > maxT) {
    maxT = t[1];
    maxIndex = 1;
  }
  if(t[2] > maxT) {
    maxT = t[2];
    maxIndex = 2;
  }
  if (maxT < 0)
    return false; //only allow intersection in front of origin count or exactly at origin

  auto intersectionPoint = origin + maxT * direction;
  for (int i = 1; i < 3; i++) {
    int dimIndex = (maxIndex + i) % 3;
    if (intersectionPoint[dimIndex] <  min[dimIndex] ||
        intersectionPoint[dimIndex] == min[dimIndex] && direction[dimIndex] < 0 ||
        intersectionPoint[dimIndex] >  max[dimIndex] ||
        intersectionPoint[dimIndex] == max[dimIndex] && direction[dimIndex] > 0)
      return false;
  }

  return true;
}

bool CudaRay::intersects(const CudaTetra tetra) const {
//  if(tetra.containsInInterior(origin))
//    return true;

  CudaTriangle triangles[4];
  tetra.getTriangles(&triangles[0]);

//  auto triangles = tetra.triangles();

  for(auto const& triangle : triangles)
    if(intersects(triangle))
      return true;

  return false;
}

__host__ __device__ bool CudaRay::intersects(const CudaTriangle triangle) const {

  CudaVector edge1, edge2, h, s, q;
  float a,f,u,v;
  edge1 = triangle.p1 - triangle.p0;
  edge2 = triangle.p2 - triangle.p0;
  h = direction.cross(edge2);
  a = edge1.dot(h);
  if (a > -gridbox::EPSILON && a < gridbox::EPSILON)
    return false;   //ray is parallel to the triangle
  f = 1.0f/a;
  s = origin - triangle.p0;
  u = f * s.dot(h);
  if (u < 0.0 || u > 1.0)
    return false;
  q = s.cross(edge1);
  v = f * direction.dot(q);
  if (v <= 0.0 || u + v >= 1.0)
    return false;
  // At this stage we can compute lambda to find out where the intersection point is on the line.
  float lambda = f * edge2.dot(q);

  if (lambda < -gridbox::EPSILON)
    return false; // intersection behind origin
  return true;
}
