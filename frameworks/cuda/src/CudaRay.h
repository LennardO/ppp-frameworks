//
// Created by lennard on 11/1/20.
//

#pragma once

#include "CudaAABox.h"
#include "CudaTetra.h"

class CudaRay {
public:
  __host__ __device__
  CudaRay(CudaPoint orgn, CudaVector drctn);
  CudaPoint origin;
  CudaVector direction;
  CudaVector mulInvDirection;

  __host__ __device__
  bool intersects(const CudaAABox aaBox) const;
  __host__ __device__
  bool intersects(const CudaTetra tetra) const;
  __host__ __device__
  bool intersects(const CudaTriangle triangle) const;
};