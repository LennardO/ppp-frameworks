//
// Created by lennard on 10/28/20.
//

#pragma once

#include "geometry/Triple.h"
#include <stdexcept>

class CudaTriple {//todo: eigene datei
public:
  float values[3];
  __host__ __device__
  CudaTriple() {};
//  __host__ __device__
//  CudaTriple(const CudaTriple& other)
//  : CudaTriple(other.x, other.y, other.z)
//  {}
  __host__ __device__
  CudaTriple(const gridbox::Triple& other)
  : CudaTriple(other._values[0], other._values[1], other._values[2])
  {}
  __host__ __device__
  CudaTriple(float x, float y, float z)
  : values{x,y,z}
  {}

  __host__ __device__
  float x() const {
    return values[0];
  }
  __host__ __device__
  float y() const {
    return values[1];
  }
  __host__ __device__
  float z() const {
    return values[2];
  }

  __host__ __device__
  float operator[](u_short i) const {
    return values[i];
  }
  __host__ __device__
  float& operator[](u_short i) {
    return values[i];
  }
};

class CudaVector : public CudaTriple {
public:
  using CudaTriple::CudaTriple;

  __host__ __device__
  float dot(const CudaVector other) const;
  __host__ __device__
  CudaVector cross(const CudaVector other) const;
};

// @brief creates a random vector with norm2 == 1
inline CudaVector randomVector();

inline bool operator==(const CudaVector lhs, const CudaVector rhs);
bool operator<(const CudaVector lhs, const CudaVector rhs);
bool operator>(const CudaVector lhs, const CudaVector rhs);

CudaVector operator+(const CudaVector lhs, const CudaVector rhs);
CudaVector operator-(const CudaVector lhs, const CudaVector rhs);
__host__ __device__
CudaVector operator*(const float scalar, const CudaVector vector);
CudaVector operator*(const CudaVector vector, const float scalar);
CudaVector operator/(const CudaVector vector, const float scalar);