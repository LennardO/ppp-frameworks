//
// Created by lennard on 10/27/20.
//

#include "OctreeGridSearch.h"
#include "CudaOctree.h"
#include "CudaTetra.h"
#include "CudaRay.h"
#include "CudaCamera.h"

#include <numeric>
#include <iostream>
#include <sys/types.h>
#include <cuda_gl_interop.h>
#include <easylogging++.h>
#include <image/PixelRGBA.h>

#define checkError(ans) { cudaAssert((ans), __FILE__, __LINE__); }
inline void cudaAssert(cudaError_t code, const char *file, int line)
{
  if (code != cudaSuccess)
  {
    std::cerr << "cuda error: " << cudaGetErrorString(code) << " in " << file << " at " << line ;
    exit(code);
  }
}

__global__
void calculateTargets(CudaTetra* tetras, int* regionIDs, size_t regionSize, CudaPoint aabMid, uint8_t* targets) {
  // blockIdx.x  = block id im grid
  // gridDim.x   = anzahl blöcke im grid
  // threadIdx.x = thread id im block
  // blockDim.x  = anzahl threads im block
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  for (int i = index; i < regionSize; i+=stride) {
    auto tetra = tetras[regionIDs[i]];
    for(u_short j=0; j<4; j++) {
      auto point = tetra.point(j);
      u_int8_t target = 0;
      for(u_short k=0; k<3; k++) {
        u_int8_t isRight = point[k] > aabMid[k];
        target += (1u<<(2u-k)) * isRight;
      }
      targets[i] |= 1u << target;
    }
    //todo: Überschneidungen mit Tetras, die keinen Punkt im Oktanten haben, werden ignoriert
  }
}

CudaPoint toCudaPoint(const gridbox::Point& point) {
  return {point.x(), point.y(), point.z()};
}

namespace gridbox {

  struct OctreeGridSearch::Impl {
    size_t nTetras = 0;
    uint maxRegionSize;
    CudaTetra* tetras_dvc = nullptr;
    float* data_dvc = nullptr;
    CudaOctree* octreeNodes_dvc;
    PrimitiveID* regions_dvc;

    cudaGraphicsResource* pboCudaResource = nullptr;
    OpenglBuffer oglBuffer;

    void split(CudaOctree&                        octree,
               std::vector<PrimitiveID>&          regions,
               std::vector<CudaOctree>&           octreeNodes,
               const std::vector<PrimitiveID>&    regionIDs);

    void identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                         const std::vector<PrimitiveID>&    regionIDs,
                         CudaPoint                          mid);
  };


  OctreeGridSearch::OctreeGridSearch()
    : _impl(std::make_unique<Impl>()) {}

//OctreeGridSearch::OctreeGridSearch(const OctreeGridSearch& other)
//: _impl(std::make_unique<Impl>(*other._impl))
//{}
//
//OctreeGridSearch::OctreeGridSearch(OctreeGridSearch&& other) = default;
//
//OctreeGridSearch& OctreeGridSearch::operator=(const OctreeGridSearch &other)
//{
//  *_impl = *other._impl;
//  return *this;
//}
//
//OctreeGridSearch& OctreeGridSearch::operator=(OctreeGridSearch&&) = default;

  OctreeGridSearch::~OctreeGridSearch() {
    if (_impl->tetras_dvc == nullptr)
      return;

    checkError(cudaFree(_impl->tetras_dvc));
    checkError(cudaFree(_impl->data_dvc));
    checkError(cudaFree(_impl->octreeNodes_dvc));
    checkError(cudaFree(_impl->regions_dvc));

//    if(_impl->pboCudaResource != nullptr)
//      checkError(cudaGraphicsUnregisterResource(_impl->pboCudaResource));
  };


  void OctreeGridSearch::construct(const gridbox::Grid& grid, u_long maxRegionSize) {
    LOG(INFO) << "running with: Cuda";

    _impl->nTetras = grid.size();
    _impl->maxRegionSize = maxRegionSize;

    static_assert(sizeof(CudaTetra) == sizeof(Tetrahedron), "CudaTetra and Tetrahedron are not the same size");
    checkError(cudaMalloc(&_impl->tetras_dvc, sizeof(CudaTetra) * _impl->nTetras));
    checkError(cudaMemcpy(_impl->tetras_dvc, grid.tetrahedronsPtr(), sizeof(CudaTetra) * _impl->nTetras,
                              cudaMemcpyHostToDevice));

    checkError(cudaMalloc(&_impl->data_dvc, sizeof(float) * _impl->nTetras));
    checkError(cudaMemcpy(_impl->data_dvc, grid.dataPtr(), sizeof(float) * _impl->nTetras,
                              cudaMemcpyHostToDevice));

    std::vector<CudaOctree> octreeNodes;

    std::vector<PrimitiveID> ids(_impl->nTetras);
    std::iota(ids.begin(), ids.end(), 0);

    CudaOctree root;
    root.aaBox = grid.aabb();
    root.regionSize = _impl->nTetras;
    octreeNodes.emplace_back(root);

    std::vector<PrimitiveID> regions;
    regions.reserve(_impl->nTetras*3);

    _impl->split(octreeNodes[0], regions, octreeNodes, ids);

    regions.shrink_to_fit();
    auto totalLeafTetras = regions.size();
    checkError(cudaMalloc(&_impl->regions_dvc, sizeof(PrimitiveID)*totalLeafTetras));
    checkError(cudaMemcpy(_impl->regions_dvc, regions.data(), sizeof(PrimitiveID)*totalLeafTetras,
                              cudaMemcpyHostToDevice));

    LOG(INFO) << "sum of all region sizes: " << totalLeafTetras
              << " -> x" << (double)totalLeafTetras/_impl->nTetras << " increase\n";
    LOG(INFO) << "total tree nodes: " << octreeNodes.size();

    checkError(cudaMalloc(&_impl->octreeNodes_dvc, sizeof(CudaOctree)*octreeNodes.size()));
    checkError(cudaMemcpy(_impl->octreeNodes_dvc, octreeNodes.data(), sizeof(CudaOctree)*octreeNodes.size(),
                              cudaMemcpyHostToDevice));
  }

  using bool8 = bool[8];

  __global__
  void identifyTargetsKernel(bool8* targets_dvc, PrimitiveID* regionIDs_dvc, CudaTetra* tetras_dvc, RegionSizeT regionSize, CudaPoint mid) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;
    for (int idx = index; idx < regionSize; idx+=stride) {
      auto tetraID = regionIDs_dvc[idx];
      auto tetra = tetras_dvc[tetraID];
//      for(auto const& point : tetra.points()) {
      for(int pID=0; pID<4; pID++) {
        auto point = tetra.point(pID);
        u_int8_t target = 0;
        for(u_short k=0; k<3; k++) {
          u_int8_t isRight = point[k] > mid[k];
          target += (1u<<(2u-k)) * isRight;
        }
        targets_dvc[idx][target] = true;
      }
    }
  }

  void OctreeGridSearch::Impl::identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                                               const std::vector<PrimitiveID>&    regionIDs,
                                               CudaPoint                          mid) {
    RegionSizeT regionSize = regionIDs.size();


    bool8* targets_dvc;
    static_assert(sizeof(bool8) == sizeof(std::array<bool, 8>), "bool8 and std::array<bool, 8> are not the same size");
//    checkError( cudaMalloc(&targets_dvc, sizeof(bool8)*regionSize) );
    checkError( cudaMallocHost(&targets_dvc, sizeof(bool8)*regionSize) );
//    checkError( cudaMemcpy(targets_dvc, targets.data(), sizeof(bool8)*regionSize,
//                               cudaMemcpyHostToDevice));
    for(RegionSizeT l=0; l<regionSize; l++)
      for(u_short i=0; i<8; i++)
        targets_dvc[l][i] = false;

    PrimitiveID* regionIDs_dvc;
    checkError( cudaMalloc(&regionIDs_dvc, sizeof(PrimitiveID)*regionSize) );
    checkError( cudaMemcpy(regionIDs_dvc, regionIDs.data(), sizeof(PrimitiveID)*regionSize,
                               cudaMemcpyHostToDevice));

    size_t blockSize = 256;
    size_t numBlocks = (regionSize + blockSize - 1) / blockSize; // aufrunden falls node.regionSize kein Mehrfaches von blockSize ist
    identifyTargetsKernel<<<numBlocks, blockSize>>>(targets_dvc, regionIDs_dvc, tetras_dvc, regionSize, mid);
    checkError(cudaPeekAtLastError());
    checkError(cudaDeviceSynchronize());

//    checkError( cudaMemcpy(targets.data(), targets_dvc, sizeof(bool8)*regionSize,
//                               cudaMemcpyDeviceToHost) );
    for(RegionSizeT l=0; l<regionSize; l++)
      for(u_short i=0; i<8; i++)
        targets[l][i] = targets_dvc[l][i];

    checkError( cudaFreeHost(targets_dvc) );
    checkError( cudaFree(regionIDs_dvc) );
  }

  void OctreeGridSearch::Impl::split(CudaOctree&                      octree,
                                     std::vector<PrimitiveID>&        regions,
                                     std::vector<CudaOctree>&         octreeNodes,
                                     const std::vector<PrimitiveID>&  regionIDs) {
    auto currentRegionSize = octree.regionSize;
    if(currentRegionSize <= maxRegionSize) {
      octree.regionStart = regions.size();
      regions.insert(std::end(regions), std::begin(regionIDs), std::end(regionIDs));
      return;
    }

    auto ownID = octreeNodes.size() -1;
    auto mid = octree.aaBox.mid();
    auto octants = octree.aaBox.octants();
    octree.leftChild = octreeNodes.size();//leftChild will be inserted next

    std::vector<std::array<bool, 8>> targets(currentRegionSize, {false});
    identifyTargets(targets, regionIDs, mid);

    auto lastChildID = octree.leftChild;

    for(u_short k=0; k<8; k++) {

      RegionSizeT childRegionSize = 0;
      std::vector<PrimitiveID> childRegionIDs;
      for(RegionSizeT i=0; i<currentRegionSize; i++)
        if(targets[i][k]) {
          childRegionIDs.emplace_back(regionIDs[i]);
          childRegionSize++;
        }
      LOG(DEBUG) << "childRegionSize: " << childRegionSize;

      CudaOctree child;
      child.aaBox = octants[k];
      child.regionSize = childRegionSize;
      child.parent = ownID;

      auto childID = octreeNodes.size();
      octreeNodes.emplace_back(child);
      octreeNodes[lastChildID].rightSibling = childID;
      lastChildID = childID;

      split(octreeNodes[childID], regions, octreeNodes, childRegionIDs);
    }
  }

  void OctreeGridSearch::setOutputBuffer(const OpenglBuffer& openglBuffer) {
    checkError(cudaGraphicsGLRegisterBuffer(&_impl->pboCudaResource, openglBuffer.id(), cudaGraphicsRegisterFlagsWriteDiscard));
    _impl->oglBuffer = openglBuffer;
  }

  __device__
  void traverseAndAdd(float& dataSum, const float& sumMax, const CudaOctree* octreeNodes_dvc, const CudaRay& ray, const PrimitiveID* regions_dvc, const CudaTetra* tetras_dvc, const float* data_dvc) {
    OctreeID nodeID = 0;
    bool goingUp = false;
    while(nodeID != INVALID_OCTREE_ID && dataSum < sumMax) {
      auto node = octreeNodes_dvc[nodeID];
      if(goingUp) {
        if (node.rightSibling != INVALID_OCTREE_ID) {
          goingUp = false;
          nodeID = node.rightSibling;
        }
        else
          nodeID = node.parent;
        continue;
      }

//      CudaPoint min = node.aaBox._min;
//      CudaPoint max = node.aaBox._max;
//      CudaAABox cudaAABox(min,max);
      CudaAABox cudaAABox(node.aaBox);
      bool intersected = ray.intersects(cudaAABox);
      bool isLeaf = node.regionStart != INVALID_PRIMITIVE_ID;

      if(isLeaf && intersected) {
        //todo: use device side lambda instead?
        for(RegionSizeT i=0; i<node.regionSize; i++) {
          auto tetraID = regions_dvc[node.regionStart + i];
          auto tetra = tetras_dvc[tetraID];
          if(ray.intersects(tetra))
            dataSum += data_dvc[tetraID];
        }
      }

      if( !isLeaf && intersected ) {
        nodeID = node.leftChild;
        continue;
      }

      bool hasRightSibling = node.rightSibling != INVALID_OCTREE_ID;
      nodeID = hasRightSibling ? node.rightSibling : node.parent;
      goingUp = !hasRightSibling;
    }
  }

  __global__
  void renderKernel(PixelRGBA*          tex_dvc,
                    uint                width,
                    uint                height,
                    const CudaCamera    camera,
                    CudaOctree*         octreeNodes_dvc,
                    PrimitiveID*        regions_dvc,
                    CudaTetra*          tetras_dvc,
                    float*              data_dvc,
                    float               xAngleOffset,
                    float               yAngleOffset) {

    uint x = blockIdx.x * blockDim.x + threadIdx.x;
    uint y = blockIdx.y * blockDim.y + threadIdx.y;
    if(x >= width || y >= height) return;
    uint linearIndex = y * width + x;//todo: use index from cuda instead?

    float xAngle = -camera.fov / 2;
    float yAngle = -camera.verticalFOV / 2;

    xAngle += x * xAngleOffset;
//    float xValue = tanf( toRadian(xAngle) ); //todo: enable with --expt-relaxed-constexpr ?
    float xValue = tanf( xAngle * (float)M_PI / 180.0f );
    yAngle += y * yAngleOffset;
//    float yValue = tanf( toRadian(yAngle) );
    float yValue = tanf( yAngle * (float)M_PI / 180.0f );

    CudaRay ray = {camera.position, {xValue, yValue, -1}};//todo: nicht nur horizontale richtungen erlauben

    float sumMax = 20000;
    float dataSum = 0;

    traverseAndAdd(dataSum, sumMax, octreeNodes_dvc, ray, regions_dvc, tetras_dvc, data_dvc);

    float intensity = dataSum / sumMax;
    intensity = intensity > 1 ? 1 : intensity;

    tex_dvc[linearIndex] = {255, (u_char) (255 - intensity * 255), (u_char) (255 - intensity * 255), 255};
  }

  void OctreeGridSearch::render(const Camera& camera) {

    checkError(cudaGraphicsMapResources(1, &_impl->pboCudaResource, nullptr));
    PixelRGBA* tex_dvc;
    checkError(cudaGraphicsResourceGetMappedPointer((void**) &tex_dvc, nullptr, _impl->pboCudaResource));

    auto width = _impl->oglBuffer.width();
    auto height = _impl->oglBuffer.height();
//    auto numPixels = width*height;

    float xAngleOffset = camera.fov() / (width - 1);
    float yAngleOffset = camera.verticalFOV() / (height - 1);

    dim3 blockDimension = {16, 16};
    uint gridWidth = (width + blockDimension.x - 1) / blockDimension.x;
    uint gridHeight = (height + blockDimension.y - 1) / blockDimension.y;
    dim3 gridDimension = {gridWidth, gridHeight};
    std::cout << "using " << gridWidth << "x" << gridHeight << " blocks with " << blockDimension.x << "x"
              << blockDimension.y << " threads\n";

    CudaCamera cudaCamera(camera);
    renderKernel<<<gridDimension, blockDimension>>>(tex_dvc, width, height, cudaCamera, _impl->octreeNodes_dvc, _impl->regions_dvc, _impl->tetras_dvc, _impl->data_dvc, xAngleOffset, yAngleOffset);
    checkError(cudaPeekAtLastError());
    checkError( cudaDeviceSynchronize() );

    checkError(cudaGraphicsUnmapResources(1, &_impl->pboCudaResource, nullptr));
  }

}