//
// Created by lennard on 11/11/20.
//

#pragma once

#include <grid/Grid.h>
#include <OpenglBuffer.h>
#include "../../../libs/scene/include/scene/Camera.h"

namespace gridbox {
  class OctreeGridSearch {
  public:
    OctreeGridSearch();                                             // Constructor
    OctreeGridSearch(const OctreeGridSearch&) = delete;             // Copy constructor
    OctreeGridSearch(OctreeGridSearch&&) = delete;                  // Move constructor
    OctreeGridSearch& operator=(const OctreeGridSearch&) = delete;  // Copy assignment operator
    OctreeGridSearch& operator=(OctreeGridSearch&&) = delete;       // Move assignment operator
    ~OctreeGridSearch();                                            // Destructor

    void construct(const Grid& grid, u_long maxRegionSize);
    void setOutputBuffer(const OpenglBuffer& openglBuffer);
    void render(const Camera& camera);

  private:
    struct Impl;
    std::unique_ptr<Impl> _impl;

  };
}