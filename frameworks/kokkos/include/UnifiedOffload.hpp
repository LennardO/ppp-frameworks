//
// Created by lennard on 12/11/20.
//

#pragma once

#include <Kokkos_Core.hpp>

namespace gridbox {
  template<typename T>
  class SequenceHandler {
  public:
    using value_type = T;

    SequenceHandler() = default;
    SequenceHandler(std::vector<T>& dataVector)
    : _hostDataPtr(dataVector.data())
    , _hostDataPtrConst(dataVector.data())
    , _size(dataVector.size())
    {}
    SequenceHandler(const std::vector<T>& dataVector)
    : _hostDataPtrConst(dataVector.data())
    , _size(dataVector.size())
    {}

    ~SequenceHandler() {
      if(_writeBack) {
        Kokkos::deep_copy(_hostDataView, _deviceDataView);
        std::memcpy(_hostDataPtr, &_hostDataView(0), sizeof(T)*_size);
      }
    }

    T* data() {
      return _hostDataPtr;
    }

    Kokkos::View<T*> getAccess_read() {
      _deviceDataView = Kokkos::View<T*>("changeMe", _size);
      _hostDataView = Kokkos::create_mirror_view(_deviceDataView);
      std::memcpy(&_hostDataView(0), _hostDataPtrConst, sizeof(T)*_size);
      Kokkos::deep_copy(_deviceDataView, _hostDataView);

      return _deviceDataView;
    }

    Kokkos::View<T*> getAccess_write() {
      _deviceDataView = Kokkos::View<T*>("changeMe2", _size);
      _hostDataView = Kokkos::create_mirror_view(_deviceDataView);
      _writeBack = true;
      return _deviceDataView;
    }

  private:
    T* _hostDataPtr = nullptr;
    const T* _hostDataPtrConst = nullptr;
    Kokkos::View<T*> _deviceDataView;
    typename Kokkos::View<T*>::HostMirror _hostDataView;
    size_t _size;
    bool _writeBack = false;
  };

  #define DEVICE_SETUP() \
    Kokkos::initialize();

  #define DEVICE_FINALIZE() \
    Kokkos::finalize();

  #define BEGIN_OFFLOAD_SETUP()

  #define END_OFFLOAD_SETUP()

  #define DEVICE_ACCESS(buffer, mode) buffer.getAccess_##mode()
  #define BEGIN_EXECUTE(N) Kokkos::parallel_for("Multiply2", N, KOKKOS_LAMBDA (const int& idx) {

  #define END_EXECUTE() });
}