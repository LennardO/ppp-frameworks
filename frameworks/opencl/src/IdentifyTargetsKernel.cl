#include "opencl/src/OpenclTetra.h"
__kernel void identifyTargetsKernel(__global OpenclTetra* tetras,
                                    __global PrimitiveID* regionIDs,
                                    __global Point mid,
                                    __global std::array<bool, 8>* targets)
{
    auto tetraID = regionIDs[get_global_id(0)];
    auto tetra = tetras[tetraID];
    for(auto const& point : tetra.points()) {
      u_int8_t target = 0;
      for(u_short k=0; k<3; k++) {
        u_int8_t isRight = point[k] > mid[k];
        target += (1u<<(2u-k)) * isRight;
      }
      targets[get_global_id(0)][target] = true;
    }
}
