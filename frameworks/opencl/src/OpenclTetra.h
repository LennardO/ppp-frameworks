//
// Created by lennard on 10/30/20.
//

#pragma once

#include "OpenclPoint.h"

struct OpenclTetra {
  OpenclPoint p0, p1, p2, p3;
  OpenclPoint point(const u_short & i) const {
    switch (i) {
      case 0:
        return p0;
      case 1:
        return p1;
      case 2:
        return p2;
      case 3:
        return p3;
      default:
        throw std::out_of_range(std::to_string(i) + " is not in range [0,3]");
    }
  }

};