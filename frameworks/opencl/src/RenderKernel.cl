__kernel void renderKernel(__global Tetrahedron* tetras,
                           __global float* data,
                           __global PrimitiveID* regions,
                           __global Octree* octreeNodes,
                           __global Camera camera,
                           __global float xAngleOffset,
                           __global float yAngleOffset,
                           __global PixelRGBA* texture)
{
    uint x = get_global_id(0);
    uint y = get_global_id(1);
    uint linearIndex = y * get_global_size(0) + x;

    float xAngle = -camera.fov() / 2;
    float yAngle = -camera.verticalFOV() / 2;

    xAngle += x * xAngleOffset;
    float xValue = tanf( toRadian(xAngle) );
    yAngle += y * yAngleOffset;
    float yValue = tanf( toRadian(yAngle) );

    Ray ray = {camera.position(), {xValue, yValue, -1}};

    float sumMax = 20000;
    float dataSum = 0;

    traverse(octreeNodes, ray, dataSum, sumMax, [&](Octree node) mutable {
      for(RegionSizeT i=0; i<node.regionSize; i++) {
        auto tetraID = regions[node.regionStart + i];
        auto tetra = tetras[tetraID];
        if(tetra.intersects(ray))
          dataSum += data_acc[tetraID];
      }
    });

    float intensity = dataSum / sumMax;
    intensity = intensity > 1 ? 1 : intensity;

    texture[linearIndex] = {255, (u_char) (255 - intensity * 255), (u_char) (255 - intensity * 255), 255};
}