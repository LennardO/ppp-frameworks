//
// Created by lennard on 11/11/20.
//

#include "OctreeGridSearch.h"
#include "TetrahedronOMP.h"

#include <algorithms/Octree.h>
#include "../../../libs/scene/include/scene/Camera.h"
#include <image/PixelRGBA.h>
#include <omp.h>

//class SplitKernel;
//class RenderKernel;

//extern "C" {
//__stack_chk_fail(void)
//{
//  ...
//}
//}

namespace gridbox {

#pragma omp declare target

  bool rayIntersectsTetraOMP(const Ray& ray, const TetrahedronOMP& tetra) {

    if(tetra.containsInInterior(ray._origin))
      return true;

//    for(auto const& triangle : tetra.triangles()) {
    for(u_short i=0; i<4; i++) {
//      auto facetIntersecLambda = triangle.intersectionLambdaInclusive(ray);
//      if ()
//        return true;
    }

    return false;
  }

  bool rayIntersectsAABoxOMP(const Ray& ray, const AABox& aaBox) {
//    auto direction = ray.direction();
//    auto origin = ray.origin();
//    auto mulInvDirection = ray.mulInvDirection();
//
//    if (aaBox.containsInInterior(origin))
//      return true;
//
//    const auto& min = aaBox.min();
//    const auto& max = aaBox.max();
//
//    std::array<fp_t, 3> t;
//    for (int i = 0; i < 3; i++) {
//      if (direction[i] > 0)
//        t[i] = (min[i] - origin[i]) * mulInvDirection[i];
//      else if (direction[i] < 0)
//        t[i] = (max[i] - origin[i]) * mulInvDirection[i];
//      else
//        t[i] = -INFINITY;
//    }
//    size_t maxIndex = std::distance(t.begin(), std::max_element(t.begin(), t.end()));
//    fp_t maxT = t[maxIndex];
//    if (maxT < 0)
//      return false; //only allow intersection in front of origin count or exactly at origin
//
//    auto intersectionPoint = origin + maxT * direction;
//    for (int i = 1; i < 3; i++) {
//      int dimIndex = (maxIndex + i) % 3;
//      if (intersectionPoint[dimIndex] <  min[dimIndex] ||
//          intersectionPoint[dimIndex] == min[dimIndex] && direction[dimIndex] < 0 ||
//          intersectionPoint[dimIndex] >  max[dimIndex] ||
//          intersectionPoint[dimIndex] == max[dimIndex] && direction[dimIndex] > 0)
//        return false;
//    }

    return true;
  }
#pragma omp end declare target

  struct OctreeGridSearch::Impl {
//    Kokkos::ScopeGuard kokkosScopeGuard;
    size_t nTetras = 0;
    uint maxRegionSize;
//    std::vector<Tetrahedron> tetras;
    TetrahedronOMP* tetrasPtr;
//    std::vector<float> data;
    float* dataPtr;
    u_long nLeafTetras;
    PrimitiveID* regionsPtr;
    u_int nNodes;
//    std::vector<Octree> octreeNodes;
    Octree* octreeNodesPtr;
//    Kokkos::View<Tetrahedron*> tetras_view;
//    Kokkos::View<float*> data_view;
//    Kokkos::View<Octree*> octreeNodes_view;
//    Kokkos::View<PrimitiveID*> regions_view;
    OpenglBuffer oglBuffer;
//    size_t nTetras = 0;
//    uint maxRegionSize;
//    sycl::buffer<Tetrahedron,1> tetras_buf;
//    sycl::buffer<float,1> data_buf;
//    sycl::buffer<Octree,1> octreeNodes_buf;
//    sycl::buffer<PrimitiveID,1> regions_buf;
//    OpenglBuffer oglBuffer;

    void split(Octree&                            octree,
               std::vector<PrimitiveID>&          regions,
               std::vector<Octree>&               octreeNodes,
               const std::vector<PrimitiveID>&    regionIDs);

    void identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                         const std::vector<PrimitiveID>&    regionIDs,
                         Point                              mid);
  };


  OctreeGridSearch::OctreeGridSearch()
  : _impl(std::make_unique<Impl>())
  {
    if(_impl->nTetras == 0)
      return;

    omp_target_free(_impl->tetrasPtr, omp_get_default_device());
    omp_target_free(_impl->dataPtr, omp_get_default_device());
    omp_target_free(_impl->regionsPtr, omp_get_default_device());
    omp_target_free(_impl->octreeNodesPtr, omp_get_default_device());

//    #pragma omp target exit data map(release: _impl->tetrasPtr[:_impl->nTetras])
//    #pragma omp target exit data map(release: _impl->dataPtr[:_impl->nTetras])
//    #pragma omp target exit data map(release: _impl->regionsPtr[:_impl->nLeafTetras])
//    #pragma omp target exit data map(release: _impl->octreeNodesPtr[:_impl->nNodes])
  }

//OctreeGridSearch::OctreeGridSearch(const OctreeGridSearch& other)
//: _impl(std::make_unique<Impl>(*other._impl))
//{}
//
//OctreeGridSearch::OctreeGridSearch(OctreeGridSearch&& other) = default;
//
//OctreeGridSearch& OctreeGridSearch::operator=(const OctreeGridSearch &other)
//{
//  *_impl = *other._impl;
//  return *this;
//}
//
//OctreeGridSearch& OctreeGridSearch::operator=(OctreeGridSearch&&) = default;

  OctreeGridSearch::~OctreeGridSearch() = default;

  std::string get_omp_version_string() {
    std::unordered_map<unsigned,std::string> map{
    {200505,"2.5"},{200805,"3.0"},{201107,"3.1"},{201307,"4.0"},{201511,"4.5"},{201811,"5.0"},{202011,"5.1"}};
    return map.at(_OPENMP);
  }

  void OctreeGridSearch::construct(const gridbox::Grid& grid, u_long maxRegionSize) {
    LOG(INFO) << omp_get_max_threads();
    LOG(INFO) << "running with: OpenMP version " << get_omp_version_string();
    LOG(INFO) << "  number of devices: " << omp_get_num_devices();
    LOG(INFO) << "  default device number: " << omp_get_default_device();
    if(omp_get_default_device() >= omp_get_num_devices())
      LOG(FATAL) << "not enough devices found";

    _impl->nTetras = grid.size();
    _impl->maxRegionSize = maxRegionSize;

    auto hostID = omp_get_initial_device();
    bool offload;
    #pragma omp target defaultmap(tofrom:scalar)
      offload = !omp_is_initial_device();
    auto deviceID = offload ? 0: omp_get_initial_device();
//    auto deviceID = omp_get_default_device();

    deviceID = hostID;

    if(sizeof(TetrahedronOMP) != sizeof(Tetrahedron))
      LOG(FATAL) << "TetrahedronOMP incompatible";
//    _impl->tetras = grid.tetrahedrons();
//    _impl->tetrasPtr = _impl->tetras.data();
//    #pragma omp target enter data map(to: _impl->tetrasPtr[:_impl->nTetras])
    _impl->tetrasPtr = (TetrahedronOMP*) omp_target_alloc(sizeof(TetrahedronOMP) * _impl->nTetras, deviceID);
    omp_target_memcpy(_impl->tetrasPtr, (void*) grid.tetrahedronsPtr(),
                      sizeof(TetrahedronOMP) * _impl->nTetras,
                      0,0,
                      deviceID, hostID);

//    _impl->data = grid.data();
//    _impl->dataPtr = _impl->data.data();
//    #pragma omp target enter data map(to: _impl->dataPtr[:_impl->nTetras])
    _impl->dataPtr = (float*) omp_target_alloc(sizeof(float) * _impl->nTetras, deviceID);
    omp_target_memcpy(_impl->dataPtr, (void*) grid.dataPtr(),
                      sizeof(float) * _impl->nTetras,
                      0,0,
                      deviceID, hostID);

    std::vector<Octree> octreeNodes;

    std::vector<PrimitiveID> ids(_impl->nTetras);
    std::iota(ids.begin(), ids.end(), 0);

    Octree root;
    root.aaBox = grid.aabb();
    root.regionSize = _impl->nTetras;
    octreeNodes.emplace_back(root);

    std::vector<PrimitiveID> regions;
    regions.reserve(_impl->nTetras*3);

    _impl->split(octreeNodes[0], regions, octreeNodes, ids);

    regions.shrink_to_fit();
    auto totalLeafTetras = regions.size();

    _impl->nLeafTetras = totalLeafTetras;
//    _impl->regionsPtr = regions.data();
//    #pragma omp target enter data map(to: _impl->regionsPtr[:_impl->nLeafTetras])
    _impl->regionsPtr = (PrimitiveID*) omp_target_alloc(sizeof(PrimitiveID) * _impl->nLeafTetras, deviceID);
    omp_target_memcpy(_impl->regionsPtr, (void*) regions.data(),
                      sizeof(PrimitiveID) * _impl->nLeafTetras,
                      0,0,
                      deviceID, hostID);

    LOG(INFO) << "sum of all region sizes: " << totalLeafTetras
              << " -> x" << (double)totalLeafTetras/_impl->nTetras << " increase\n";
    LOG(INFO) << "total tree nodes: " << octreeNodes.size();

    _impl->nNodes = octreeNodes.size();
//    _impl->octreeNodesPtr = _impl->octreeNodes.data();
//    #pragma omp target enter data map(to: _impl->octreeNodesPtr[:_impl->nNodes])
    _impl->octreeNodesPtr = (Octree*) omp_target_alloc(sizeof(Octree) * _impl->nNodes, deviceID);
    omp_target_memcpy(_impl->tetrasPtr, (void*) grid.tetrahedronsPtr(),
                      sizeof(Octree) * _impl->nNodes,
                      0,0,
                      deviceID, hostID);

  }

  void OctreeGridSearch::Impl::identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                                               const std::vector<PrimitiveID>&    regionIDs,
                                               Point                              mid) {
    u_long regionSize = regionIDs.size();

    std::array<bool, 8>* targetsPtr = targets.data();
    auto regionIDsPtr = regionIDs.data();

    #pragma omp target map(tofrom: targetsPtr[:regionSize]) map(to: regionIDsPtr[:regionSize])
      #pragma omp for
        for(u_int l=0; l<regionSize; l++) {
          auto tetraID = regionIDsPtr[l];
          auto tetra = tetrasPtr[tetraID];
          for(auto const& point : tetra.points) {
            u_int8_t target = 0;
            for(u_short k=0; k<3; k++) {
              u_int8_t isRight = point[k] > mid[k];
              target += (1u<<(2u-k)) * isRight;
            }
            targetsPtr[l][target] = true;
          }
        }
  }


  void OctreeGridSearch::Impl::split(Octree&                          octree,
                                     std::vector<PrimitiveID>&        regions,
                                     std::vector<Octree>&             octreeNodes,
                                     const std::vector<PrimitiveID>&  regionIDs) {

    auto currentRegionSize = octree.regionSize;
    if(currentRegionSize <= maxRegionSize) {
      octree.regionStart = regions.size();
      regions.insert(std::end(regions), std::begin(regionIDs), std::end(regionIDs));
      return;
    }

    auto ownID = octreeNodes.size() -1;
    auto mid = octree.aaBox.mid();
    auto octants = octree.aaBox.octants();
    octree.leftChild = octreeNodes.size();//leftChild will be inserted next

    std::vector<std::array<bool, 8>> targets(currentRegionSize, {false});
    identifyTargets(targets, regionIDs, mid);

    auto lastChildID = octree.leftChild;

    for(u_short k=0; k<8; k++) {

      RegionSizeT childRegionSize = 0;
      std::vector<PrimitiveID> childRegionIDs;
      for(RegionSizeT i=0; i<currentRegionSize; i++)
        if(targets[i][k]) {
          childRegionIDs.emplace_back(regionIDs[i]);
          childRegionSize++;
        }

      Octree child;
      child.aaBox = octants[k];
      child.regionSize = childRegionSize;
      child.parent = ownID;

      auto childID = octreeNodes.size();
      octreeNodes.emplace_back(child);
      octreeNodes[lastChildID].rightSibling = childID;
      lastChildID = childID;

      split(octreeNodes[childID], regions, octreeNodes, childRegionIDs);
    }
  }

  void OctreeGridSearch::setOutputBuffer(const OpenglBuffer& openglBuffer) {
    _impl->oglBuffer = openglBuffer;
  }

  void OctreeGridSearch::render(const Camera& camera) {

    PixelRGBA* texPtr = (PixelRGBA*) glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);

    auto width = _impl->oglBuffer.width();
    auto height = _impl->oglBuffer.height();
    auto numPixels = width*height;

    float xAngleOffset = camera.fov() / (width - 1);
    float yAngleOffset = camera.verticalFOV() / (height - 1);

    auto octreeNodesPtr = _impl->octreeNodesPtr;
    auto regionsPtr = _impl->regionsPtr;
    auto tetrasPtr = _impl->tetrasPtr;
    auto dataPtr = _impl->dataPtr;

    #pragma omp target map(from: texPtr[:numPixels])
      #pragma omp for
        for(uint l=0; l<numPixels; l++) {
          uint x = l % width;
          uint y = l / width;
          uint linearIndex = l;

          float xAngle = -camera.fov() / 2;
          float yAngle = -camera.verticalFOV() / 2;

          xAngle += x * xAngleOffset;
          float xValue = tanf( toRadian(xAngle) );
          yAngle += y * yAngleOffset;
          float yValue = tanf( toRadian(yAngle) );

          Ray ray = {camera.position(), {xValue, yValue, -1}};

          float sumMax = 20000;
          float dataSum = 0;

////          traverse(_impl->octreeNodesPtr, ray, dataSum, sumMax, [&](Octree node) mutable {
////            for(RegionSizeT i=0; i<node.regionSize; i++) {
////              auto tetraID = _impl->regionsPtr[node.regionStart + i];
////              auto tetra = _impl->tetrasPtr[tetraID];
////              if(tetra.intersects(ray))
////                dataSum += _impl->dataPtr[tetraID];
////            }
////          });

          OctreeID nodeID = 0;
          bool goingUp = false;
          while(nodeID != INVALID_OCTREE_ID && dataSum < sumMax) {
            auto node = octreeNodesPtr[nodeID];
            if(goingUp) {
              if (node.rightSibling != INVALID_OCTREE_ID) {
                goingUp = false;
                nodeID = node.rightSibling;
              }
              else
                nodeID = node.parent;
              continue;
            }

            bool intersected = ray.intersects(node.aaBox);
//            bool intersected = rayIntersectsAABoxOMP(ray, node.aaBox);
            bool isLeaf = node.regionStart != INVALID_PRIMITIVE_ID;

            if(isLeaf && intersected) {
              for(RegionSizeT i=0; i<node.regionSize; i++) {
              auto tetraID = regionsPtr[node.regionStart + i];
              auto tetra = tetrasPtr[tetraID];
              if(tetra.intersects(ray))
//              if(rayIntersectsTetraOMP(ray, tetra))
                dataSum += dataPtr[tetraID];
            }
            }

            if( !isLeaf && intersected ) {
              nodeID = node.leftChild;
              continue;
            }

            bool hasRightSibling = node.rightSibling != INVALID_OCTREE_ID;
            nodeID = hasRightSibling ? node.rightSibling : node.parent;
            goingUp = !hasRightSibling;
          }






          float intensity = dataSum / sumMax;
          intensity = intensity > 1 ? 1 : intensity;

          texPtr[linearIndex] = {255, (u_char) (255 - intensity * 255), (u_char) (255 - intensity * 255), 255};
        }

    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);

  }
}