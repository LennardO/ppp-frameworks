//
// Created by lennard on 12/3/20.
//

#pragma once

#include "TriangleOMP.h"

namespace gridbox {
  class TetrahedronOMP {
  public:
    Point points[4];
#pragma omp declare target

//    std::array<TriangleOMP, 4> triangles() const {
//      return {{
//                {points[1],points[2],points[3]},
//                {points[0],points[3],points[2]},
//                {points[0],points[1],points[3]},
//                {points[0],points[2],points[1]}
//              }};
//    }

    TriangleOMP triangle(u_short i) const {
      TriangleOMP triangles[4] = {
        {points[1],points[2],points[3]},
        {points[0],points[3],points[2]},
        {points[0],points[1],points[3]},
        {points[0],points[2],points[1]}
      };
      return triangles[i];
    }
    
    bool containsInInterior(const Point& point) const {
//      for(auto const& triangle : triangles())
    for(u_short i=0; i<4; i++)
        if(!triangle(i).plane().isLeft(point))
          return false;
      return true;
    }

    [[nodiscard]] bool intersects(const Ray& ray) const {
      std::set<fp_t> result;

      if(containsInInterior(ray.origin()))
        return true;

      for(u_short i=0; i<4; i++)
        if (triangle(i).intersectsInclusive(ray))
          return true;

      return false;
    }
#pragma omp end declare target
  };
}