//
// Created by lennard on 12/11/20.
//

#pragma once

#include <SYCL/sycl.hpp>

namespace gridbox {
  template<typename T>
  class SequenceHandler {
//  private:
  public:
    sycl::buffer<T, 1> _buffer;
  public:
    SequenceHandler() = default;
    SequenceHandler(const std::vector<T>& dataVector)
    : _buffer(dataVector.data(), dataVector.size())
    {}

    ~SequenceHandler() {
      std::cout << "destructing buffer\n";
      _buffer.~buffer();
    }

    sycl::accessor<T, 1, sycl::access::mode::read, sycl::access::target::global_buffer>
    getReadAccess(sycl::handler& cgh) {
      return _buffer.get_access<sycl::access::mode::read>(cgh);
    }

    sycl::accessor<T, 1, sycl::access::mode::write, sycl::access::target::global_buffer>
    getWriteAccess(sycl::handler& cgh) {
      return _buffer.get_access<sycl::access::mode::write>(cgh);
    }

  };

  #define DEVICE_SETUP()
  #define DEVICE_FINALIZE()

  #define BEGIN_OFFLOAD_SETUP() \
    sycl::queue q; \
    q.submit([&](sycl::handler& cgh) {

  #define END_OFFLOAD_SETUP() \
    });

  #define DEVICE_ACCESS(buffer, rwMode) buffer._buffer.get_access<sycl::access::mode::rwMode>(cgh)

  #define BEGIN_EXECUTE(N) cgh.parallel_for<class DummyClass>(sycl::range<1>(N), [=](sycl::id<1> idx) {

  #define END_EXECUTE() });
}