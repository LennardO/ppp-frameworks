//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Point.h"
#include "AARectangle.h"

namespace gb {

  class AABox;
  using Octants = std::array<AABox,8>;

  class AABox {
  public:
    AABox();
    AABox(Point  min, Point  max);

    [[nodiscard]] const Point& min() const;
    [[nodiscard]] const Point& max() const;
    [[nodiscard]] Point mid() const;
    [[nodiscard]] std::array<Point, 8> points() const;

    // SWB, SWF, NWB, NWF, SEB, SEF, NEB, NEF
    [[nodiscard]] Octants octants() const;
    [[nodiscard]] std::array<AARectangle, 6> facets() const;

    [[nodiscard]] bool containsInInterior(const Point& point) const;

  private:
    Point _min, _max;
  };
}
