//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Point.h"
#include "Vector.h"

namespace gb {

  class Plane {
  public:
    Plane();
    Plane(Point  support, Vector  normal);
    Plane(const Point& point0CCW, const Point& point1CCW, const Point& point2CCW);

    [[nodiscard]] const Point& support() const;

    [[nodiscard]] const Vector& normal() const;

    [[nodiscard]] bool isLeft(const Point& point) const;
    [[nodiscard]] bool isLeftOrOn(const Point& point) const;

  private:
    Point _support;
    Vector _normal;
  };
}
