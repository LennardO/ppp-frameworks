//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Triple.h"

namespace gb {
  class Point : public Triple {
  public:
    using Triple::Triple;

    friend bool operator==(const Point& lhs, const Point& rhs);
    friend Point operator+(const Point& lhs, const Vector& rhs);
    friend Vector operator-(const Point& lhs, const Point& rhs);
  };

  Point middle(const Point& lhs, const Point& rhs);
}
