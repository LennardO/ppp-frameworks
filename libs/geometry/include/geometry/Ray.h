//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Point.h"
#include "Vector.h"
#include "Tetrahedron.h"
#include "Sphere.h"
#include <vector>
#include <optional>

namespace gb {

  struct RayIntersection {
    fp_t lambda;
    Point point;
  };

  struct OptionalRaySphereIntersectionLambdas {
    fp_t in = -1;
    fp_t out = -1;
    operator bool() const {
      return in != -1;
    }
  };

  enum RayConvexPolyIntersectionType {
    Tangential,
    Proper,
    Segment,
    OriginInside,
    OriginOnBoundary
  };

  class Ray {
  public:
    Ray();
    Ray(const Point& origin, const Vector& direction);

    [[nodiscard]] const Point& origin() const;
    [[nodiscard]] const Vector& direction() const;
    [[nodiscard]] const Vector& mulInvDirection() const;

    /// @brief returns a point on the ray with distance lambda from the origin
    [[nodiscard]] Point at(const fp_t& lambda) const;
    [[nodiscard]] std::vector<Point> at(const std::vector<fp_t>& lambdas) const;

    [[nodiscard]] std::optional<RayIntersection> intersectionIn(const AABox& aaBox) const;
    [[nodiscard]] bool intersects(const AABox& aaBox) const;

    //only proper intersection
    [[nodiscard]] OptionalRaySphereIntersectionLambdas intersectionLambdas(const Sphere& sphere) const;

  private:
    Point _origin;
    Vector _direction;
    Vector _mulInvDirection;
  };
}