//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Plane.h"
#include "AABox.h"
#include "Triangle.h"
#include <vector>

namespace gb {
   class Tetrahedron {
   public:
     Tetrahedron();
     Tetrahedron(std::array<Point,4>  points);
     Tetrahedron(Point p0, Point p1, Point  p2, Point p3);

     [[nodiscard]] const std::array<Point, 4>& points() const;
     [[nodiscard]] std::array<LineSegment, 6> lineSegments() const;
     [[nodiscard]] std::array<Triangle, 4> triangles() const;

     [[nodiscard]] bool containsInInterior(const Point& point) const;
     [[nodiscard]] bool intersects(const AABox& aaBox) const;
     [[nodiscard]] bool intersects(const AARectangle& aaRec) const;
     [[nodiscard]] bool intersects(const Ray& ray) const;
     [[nodiscard]] std::vector<fp_t> intersectionLambdasInclusive(const Ray& ray) const;
     [[nodiscard]] std::vector<fp_t> intersectionLambdasInclusive(const LineSegment& ls) const;
     [[nodiscard]] std::vector<Point> intersectionPointsInclusive(const LineSegment& ls) const;
   private:
     std::array<Point,4> _p;
   };
}