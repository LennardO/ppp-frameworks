//
// Created by lennard on 10/15/20.
//

#pragma once

#include "LineSegment.h"
#include <optional>

namespace gb {

  class Triangle {
  public:
    Triangle();
    Triangle(const Point& pointA, const Point& pointB, const Point& pointC);
    [[nodiscard]] const Point& pointA() const;
    [[nodiscard]] const Point& pointB() const;
    [[nodiscard]] const Point& pointC() const;
    [[nodiscard]] std::array<Point,3> points() const;

    [[nodiscard]] std::array<LineSegment, 3> lineSegments() const;
    [[nodiscard]] Plane plane() const;

    [[nodiscard]] std::optional<fp_t> intersectionLambdaInclusive(const Ray& ray) const;

  private:
    Point _pointA, _pointB, _pointC;
  };
}
