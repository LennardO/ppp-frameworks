//
// Created by lennard on 10/15/20.
//
#pragma once

#include "ForwardDeclaration.h"
#include <array>
#include <iostream>

namespace gb {
  class Triple {
  public:
    ENABLE_DEVICE
    Triple();
    ENABLE_DEVICE
    explicit Triple(std::array<fp_t,3>  values);
    ENABLE_DEVICE
    Triple(const fp_t& x, const fp_t& y, const fp_t& z);

    [[nodiscard]] std::array<fp_t,3> toArray() const;
    [[nodiscard]] fp_t at(const u_short& i) const;
    [[nodiscard]] fp_t x() const;
    [[nodiscard]] fp_t y() const;
    [[nodiscard]] fp_t z() const;

    ENABLE_DEVICE
    fp_t operator[](u_short i) const;
    ENABLE_DEVICE
    fp_t& operator[](u_short i);

    friend std::ostream& operator<<(std::ostream& o, const Triple& triple);

    struct lex_compare {
      bool operator() (const Triple& lhs, const Triple& rhs) const;
    };

  protected:
    std::array<fp_t,3> _values;
    [[nodiscard]] bool equals(const Triple& other) const;
  };
}
