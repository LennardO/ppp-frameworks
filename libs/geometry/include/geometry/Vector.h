//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Triple.h"

namespace gb {

  class Vector : public Triple {
  public:
    using Triple::Triple;

    [[nodiscard]] fp_t norm2() const;
    /// @brief aka norm2 squared
    [[nodiscard]] fp_t squaredSum() const;

    [[nodiscard]] bool isParallel(const Vector& other) const;
    [[nodiscard]] fp_t dot(const Vector& other) const;
    [[nodiscard]] Vector cross(const Vector& other) const;
    [[nodiscard]] Vector normalized() const;

    Vector operator-() const;
    Vector& operator+=(const Vector& other);
    Vector& operator*=(const fp_t& t);
    Vector& operator/=(const fp_t& t);

    friend bool operator==(const Vector& lhs, const Vector& rhs);
    friend bool operator<(const Vector& lhs, const Vector& rhs);
    friend bool operator>(const Vector& lhs, const Vector& rhs);

    friend Vector operator+(const Vector& lhs, const Vector& rhs);
    friend Vector operator-(const Vector& lhs, const Vector& rhs);
    friend Vector operator*(const fp_t& scalar, const Vector& vector);
    friend Vector operator*(const Vector& vector, const fp_t& scalar);
    friend Vector operator/(const Vector& vector, const fp_t& scalar);
  };

  /// @brief creates a random vector with norm2 == 1
  inline Vector randomVector();

}
