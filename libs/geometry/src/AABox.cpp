//
// Created by lennard on 10/15/20.
//

#include "geometry/AABox.h"

#include <utility>

namespace gb {

  AABox::AABox() = default;
  AABox::AABox(Point  min, Point  max)
  : _min(min)
  , _max(max)
  {}

  const Point& AABox::min() const {
    return _min;
  }

  const Point& AABox::max() const {
    return _max;
  }

  Point AABox::mid() const {
    return middle(_min, _max);
  }

  std::array<Point, 8> AABox::points() const {
    return {
      _min, {_max[0],_min[1],_min[2]}, {_max[0],_max[1],_min[2]}, {_min[0],_max[1],_min[2]},
      {_min[0],_min[1],_max[2]}, {_max[0],_min[1],_max[2]}, _max, {_min[0],_max[1],_max[2]}
    };
  }

  Octants AABox::octants() const {

    auto _mid = mid();

    Point wmBack  = {_min[0],_mid[1],_min[2]};
    Point mnMid   = {_mid[0],_max[1],_mid[2]};
    Point wsMid   = {_min[0],_min[1],_mid[2]};
    Point mmFront = {_mid[0],_mid[1],_max[2]};
    Point wmMid   = {_min[0],_mid[1],_mid[2]};
    Point mnFront = {_mid[0],_max[1],_max[2]};
    Point msBack  = {_mid[0],_min[1],_min[2]};
    Point emMid   = {_max[0],_mid[1],_mid[2]};
    Point msMid   = {_mid[0],_min[1],_mid[2]};
    Point emFront = {_max[0],_mid[1],_max[2]};
    Point mmBack  = {_mid[0],_mid[1],_min[2]};
    Point enMid   = {_max[0],_max[1],_mid[2]};

    return {{
              {_min,    _mid},
              {wsMid,  mmFront},
              {wmBack, mnMid},
              {wmMid,  mnFront},
              {msBack, emMid},
              {msMid,  emFront},
              {mmBack, enMid},
              {_mid,    _max}
            }};
  }

  std::array<AARectangle, 6> AABox::facets() const {
    Point swBack  = _min;
    Point seBack  = {_max[0],_min[1],_min[2]};
    Point neBack  = {_max[0],_max[1],_min[2]};
    Point nwBack  = {_min[0],_max[1],_min[2]};
    Point swFront = {_min[0],_min[1],_max[2]};
    Point seFront = {_max[0],_min[1],_max[2]};
    Point neFront = _max;
    Point nwFront = {_min[0],_max[1],_max[2]};
    return {{
              {0,true, swBack, nwFront}, //west
              {0,false,seBack, neFront}, //east
              {1,true, swBack, seFront}, //south
              {1,false,nwBack, neFront}, //north
              {2,true, swBack, neBack},  //back
              {2,false,swFront,neFront}, //front
            }};
  }

  bool AABox::containsInInterior(const Point& point) const {
    for(u_short d=0; d<3; d++)
      if( _min[d] >= point[d]  ||  _max[d] <= point[d] )
        return false;

    return true;
  }

}