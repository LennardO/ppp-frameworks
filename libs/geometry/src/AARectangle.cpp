//
// Created by lennard on 10/17/20.
//

#include "geometry/AARectangle.h"
#include <utility>
#include <optional>

namespace gb {

  AARectangle::AARectangle(u_short fixedDim, bool fixedIsMin, Point min, Point max)
  : _fixedDim(fixedDim)
  , _fixedIsMin(fixedIsMin)
  , _min(std::move(min))
  , _max(std::move(max))
  {}

  u_short AARectangle::fixedDim() const {
    return _fixedDim;
  }

  bool AARectangle::fixedIsMin() const {
    return _fixedIsMin;
  }

  const Point& AARectangle::min1() const {
    return _min;
  }

  const Point& AARectangle::max1() const {
    return _max;
  }

  std::array<Point, 4> AARectangle::points() const {
    Point l = _min;
    l[(_fixedDim +1) % 3] = _max[(_fixedDim +1) % 3];
    Point m = _min;
    m[(_fixedDim +2) % 3] = _max[(_fixedDim +2) % 3];
    return {_min, l, _max, m};
  }

  std::array<LineSegment, 4> AARectangle::lineSegments() const {
    auto corners = points();
    return {{
              { corners[0], corners[1] },
              { corners[1], corners[2] },
              { corners[2], corners[3] },
              { corners[3], corners[0] }
            }};
  }

  std::optional<RayIntersection> AARectangle::intersection(const Ray& ray) const {
    auto ri = intersectionInclusive(ray);
    if(!ri)
      return {};
    auto iPoint = ri.value().point;
    for(u_short d=1; d<3; d++) {
      u_short idx = (_fixedDim + d) % 3;
      if( iPoint[idx] == _min[idx] || iPoint[idx] == _max[idx] )
        return {};
    }
    return ri;
  }

  std::optional<RayIntersection> AARectangle::intersectionInclusive(const Ray& ray) const {
    Point rayOrigin = ray.origin();
    Vector rayDir = ray.direction();
    fp_t fixedVal = _min[_fixedDim];

    if(rayDir[_fixedDim] == 0)
      return {};

    RayIntersection result;
    result.lambda = (fixedVal - rayOrigin[_fixedDim]) / rayDir[_fixedDim];
    if( result.lambda < -EPSILON)
      return {};
    result.point = ray.at( result.lambda );
    for(u_short d=1; d<3; d++) {
      u_short idx = (_fixedDim + d) % 3;
      if (   result.point[idx] < _min[idx]
             || result.point[idx] > _max[idx])
        return {};
    }

    return result;
  }


  std::optional<Point> AARectangle::intersectionPointInclusive(const LineSegment& ls) const {
    auto ray = ls.lsRay();
    auto ri = intersectionInclusive(ray);
    if(ri && ri.value().lambda <= 1)
      return ri.value().point;
    return {};
  }

}