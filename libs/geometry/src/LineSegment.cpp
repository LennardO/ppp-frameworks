//
// Created by lennard on 10/15/20.
//

#include "geometry/LineSegment.h"

#include <utility>

namespace gb {
  LineSegment::LineSegment() = default;
  LineSegment::LineSegment(Point  p, Point  q)
  : _p(std::move(p))
  , _q(std::move(q))
  {}

  const Point& LineSegment::p() const {
    return _p;
  }

  const Point& LineSegment::q() const {
    return _q;
  }

  Ray LineSegment::lsRay() const {
    return {_p, _q-_p};
  }

  bool LineSegment::parallel(const LineSegment& other) const {
    return (_p-_q).isParallel(other.p() - other.q());
  }

}