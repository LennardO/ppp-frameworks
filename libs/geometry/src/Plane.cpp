//
// Created by lennard on 10/15/20.
//

#include "geometry/Plane.h"

#include <utility>

namespace gb {

  Plane::Plane() = default;
  Plane::Plane(Point  support, Vector  normal)
  : _support(std::move(support))
  , _normal(std::move(normal))
  {}

  Plane::Plane(const Point& point0CCW, const Point& point1CCW, const Point& point2CCW)
  : Plane(point0CCW, (point1CCW - point0CCW).cross(point2CCW - point0CCW))
  {}

  const Point& Plane::support() const {
    return _support;
  }

  const Vector& Plane::normal() const {
    return _normal;
  }

  bool Plane::isLeft(const Point& point) const {
    auto supportPointVec = point - _support;
    return supportPointVec.dot(_normal) < -EPSILON; //true -> angle between normal and dir > 90
  }

  bool Plane::isLeftOrOn(const Point& point) const {
    auto supportPointVec = point - _support;
    return supportPointVec.dot(_normal) < EPSILON; //true -> angle between normal and dir >= 90
  }


}