//
// Created by lennard on 10/15/20.
//

#include "geometry/Ray.h"
#include <optional>
#include <algorithm> //for generate_n
#include <math.h>

namespace gb {
  Ray::Ray() = default;
  Ray::Ray(const Point& origin, const Vector& direction)
  : _origin(origin)
  , _direction(direction)
  {
    _mulInvDirection = {1.0f/_direction[0], 1.0f/_direction[1], 1.0f/_direction[2]};//todo: prüfen wieviel die Vorberechnung bringt
  }

  const Point& Ray::origin() const {
    return _origin;
  }

  const Vector& Ray::direction() const {
    return _direction;
  }

  const Vector& Ray::mulInvDirection() const {
    return _mulInvDirection;
  }

  Point Ray::at(const fp_t& lambda) const {
    return _origin + lambda * _direction;
  }

  std::vector<Point> Ray::at(const std::vector<fp_t>& lambdas) const {
    std::vector<Point> result;
    result.reserve(lambdas.size());

    std::generate_n(std::back_inserter(result), lambdas.size(),
                    [i=0,this,&lambdas]() mutable { return this->at(lambdas[i++]); });
    return result;
  }

  std::optional<RayIntersection> Ray::intersectionIn(const AABox& aaBox) const {
    if (aaBox.containsInInterior(_origin))
      return {{0.0, _origin}};

    const auto& min = aaBox.min();
    const auto& max = aaBox.max();

    std::array<fp_t, 3> t;
    for (int i = 0; i < 3; i++) {
      if (_direction[i] > 0)
        t[i] = (min[i] - _origin[i]) * _mulInvDirection[i];
      else if (_direction[i] < 0)
        t[i] = (max[i] - _origin[i]) * _mulInvDirection[i];
      else
        t[i] = -INFINITY;
    }
    size_t maxIndex = std::distance(t.begin(), std::max_element(t.begin(), t.end()));
    fp_t maxT = t[maxIndex];
    if (maxT < 0)
      return std::nullopt; //only allow intersection in front of origin count or exactly at origin

    auto intersectionPoint = _origin + maxT * _direction;
    for (int i = 1; i < 3; i++) {
      size_t dimIndex = (maxIndex + i) % 3;
      if (intersectionPoint[dimIndex]  <  min[dimIndex] ||
          (intersectionPoint[dimIndex] == min[dimIndex] && _direction[dimIndex] < 0) ||
          intersectionPoint[dimIndex]  >  max[dimIndex] ||
          (intersectionPoint[dimIndex] == max[dimIndex] && _direction[dimIndex] > 0))
        return std::nullopt;
    }

    return {{maxT, intersectionPoint}};
  }

  bool Ray::intersects(const AABox& aaBox) const {
//    return intersectionIn(aaBox).has_value();
    return intersectionIn(aaBox).operator bool();
  }

  OptionalRaySphereIntersectionLambdas Ray::intersectionLambdas(const Sphere& sphere) const {
    //sphere equation: (P - C)² = r²                          , P=PointOnSphere C=Center
    //with ray as P:   (A + tB - C)² = r²                     , Ray=A+tB
    //transforms to:   t²B² + t2B(A-C) + (A-C)² - r² = 0

    auto ac = _origin - sphere.center();
    auto a = _direction.squaredSum();
    auto bHalf = ac.dot(_direction);
    auto c = ac.squaredSum() - sphere.radius()*sphere.radius();
    auto discriminant = bHalf * bHalf - a - c;

    if(discriminant < EPSILON) return {};
    auto sqrtDiscriminant = std::sqrt(discriminant);
    OptionalRaySphereIntersectionLambdas result;
    result.in = (-bHalf - sqrtDiscriminant) / a;
    if(result.in < -EPSILON)
      return {};
    result.out = (-bHalf + sqrtDiscriminant) / a;
    return result;
  }

}