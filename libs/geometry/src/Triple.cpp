//
// Created by lennard on 10/15/20.
//
#include "geometry/Triple.h"

namespace gb {
  Triple::Triple()
  : _values({0,0,0})
  {}
  Triple::Triple(std::array<fp_t,3>  values)
  : _values(values)
  {}
  Triple::Triple(const fp_t& x, const fp_t& y, const fp_t& z)
  : _values({x,y,z})
  {}

  std::array<fp_t, 3> Triple::toArray() const {
    return _values;
  }
  fp_t Triple::at(const u_short& i) const {
    return _values[i];
  }
  fp_t Triple::x() const {
    return _values[0];
  }
  fp_t Triple::y() const {
    return _values[1];
  }
  fp_t Triple::z() const {
    return _values[2];
  }

  fp_t Triple::operator[](u_short i) const {
    return _values[i];
  }
  fp_t& Triple::operator[](u_short i) {
    return _values[i];
  }

  bool Triple::lex_compare::operator()(const Triple& lhs, const Triple& rhs) const {
    return std::lexicographical_compare(std::begin(lhs._values), std::end(lhs._values),
                                        std::begin(rhs._values), std::end(rhs._values));
  }

  std::ostream& operator<<(std::ostream& o, const Triple& triple) {
    o << "(" << triple._values[0] << "," << triple._values[1] << "," << triple._values[2] << ")";
    return o;
  }

  bool Triple::equals(const Triple& other) const {
    return _values == other._values;
  }
}
