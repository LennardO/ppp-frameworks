//
// Created by lennard on 10/15/20.
//

#include "geometry/Vector.h"
#include <math.h>
#include <numeric> //for inner_product

namespace gb {

  fp_t Vector::norm2() const {
    return std::sqrt(squaredSum());
  }
  fp_t Vector::squaredSum() const {
    return _values[0]*_values[0] + _values[1]*_values[1] + _values[2]*_values[2];
  }

  bool Vector::isParallel(const Vector& other) const {
    fp_t scalar = dot(other);
    fp_t diff = scalar*scalar - squaredSum() * other.squaredSum();
    return diff < EPSILON && diff > -EPSILON;
  }
  fp_t Vector::dot(const Vector& other) const {
    return std::inner_product(std::begin(_values), std::end(_values),
                              std::begin(other._values),
                              (fp_t)0.0);
  }

  Vector Vector::cross(const Vector& other) const {
    return {
      _values[1] * other._values[2] - _values[2] * other._values[1],
      _values[2] * other._values[0] - _values[0] * other._values[2],
      _values[0] * other._values[1] - _values[1] * other._values[0]
    };
  }

  Vector Vector::normalized() const {
    return *this / norm2();
  }


  Vector Vector::operator-() const {
    return *this * -1;
  }

  Vector& Vector::operator+=(const Vector& other) {
    _values[0] += other._values[0];
    _values[1] += other._values[1];
    _values[2] += other._values[2];
    return *this;
  }

  Vector& Vector::operator*=(const fp_t& t) {
    _values[0] += t;
    _values[1] += t;
    _values[2] += t;
    return *this;
  }

  Vector& Vector::operator/=(const fp_t& t) {
    _values[0] /= t;
    _values[1] /= t;
    _values[2] /= t;
    return *this;
  }

  /// @brief creates a random vector with norm2 == 1
  inline Vector randomVector() {
    fp_t theta = 2.0 * M_PI * tt::randomFloat01();
    fp_t phi = acos(1.0 - 2.0 * tt::randomFloat01());
    fp_t x = sin(phi) * cos(theta);
    fp_t y = sin(phi) * sin(theta);
    fp_t z = cos(phi);
    return {x,y,z};
  }

  inline bool operator==(const Vector& lhs, const Vector& rhs) {
    return lhs.equals(rhs);
  }
  bool operator<(const Vector& lhs, const Vector& rhs) {
    return lhs.squaredSum() < rhs.squaredSum();
  }
  bool operator>(const Vector& lhs, const Vector& rhs) {
    return lhs.squaredSum() > rhs.squaredSum();
  }

  Vector operator+(const Vector& lhs, const Vector& rhs) {
    return {lhs[0]+rhs[0], lhs[1]+rhs[1], lhs[2]+rhs[2]};
  }
  Vector operator-(const Vector& lhs, const Vector& rhs) {
    return {lhs[0]-rhs[0], lhs[1]-rhs[1], lhs[2]-rhs[2]};
  }
  Vector operator*(const fp_t& scalar, const Vector& vector) {
    return {scalar*vector[0], scalar*vector[1], scalar*vector[2]};
  }
  Vector operator*(const Vector& vector, const fp_t& scalar) {
    return scalar * vector;
  }
  Vector operator/(const Vector& vector, const fp_t& scalar) {
    return {vector[0]/scalar, vector[1]/scalar, vector[2]/scalar};
  }
}