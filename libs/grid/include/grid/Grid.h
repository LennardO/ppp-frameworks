//
// Created by lennard on 10/16/20.
//

#pragma once

#include "geometry/Tetrahedron.h"
#include "geometry/AABox.h"
#include <filesystem>

namespace gb {

  class Grid {
  public:

    [[nodiscard]] size_t size() const;
    [[nodiscard]] const std::vector<Tetrahedron>& tetrahedrons() const;
    [[nodiscard]] const Tetrahedron* tetrahedronsPtr() const;
    [[nodiscard]] const Tetrahedron& tetrahedron(const PrimitiveID& id) const;
    [[nodiscard]] const std::vector<float>& data() const;
    [[nodiscard]] const float* dataPtr() const;
    [[nodiscard]] float data(const PrimitiveID& id) const;
    [[nodiscard]] AABox aabb() const;

    void loadFromFile(const std::filesystem::path& filePath);
    [[nodiscard]] std::string filePath() const;

  private:
    size_t _size = 0;
    std::vector<Tetrahedron> _tetrahedrons;
    Tetrahedron* _tetrahedronsPtr = nullptr;
    std::vector<float> _data;
    float* _dataPtr = nullptr;
    AABox _aabb;
    std::string _filePath;

    void calculateAABB();
  };
}
