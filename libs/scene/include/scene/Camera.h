//
// Created by lennard on 11/1/20.
//

#pragma once

#include <geometry/AABox.h>

namespace gb {

  class Camera {
  public:
    [[nodiscard]] const Point& position() const;
    [[nodiscard]] const Vector& direction() const;
    [[nodiscard]] float fov() const;
    [[nodiscard]] float verticalFOV() const;
    [[nodiscard]] float aspectRatio() const;

    void setPosition(const Point& position);
    void setDirection(const Vector& direction);
    void setFov(float fov);
    void setAspectRatio(float aspectRatio);

    void targetFullFrame(const AABox& aaBox);

  private:
    Point _position = {0,0,0};
    Vector _direction = {1,1,1};
    float _fov = 70;
    float _verticalFOV = 70;
    float _aspectRatio = 1;

    void updateVerticalFOV();
  };
}
