//
// Created by lennard on 3/10/21.
//

#pragma once

namespace tt {

//  constexpr float toRadian(const float& angleDegree);
  float toRadian(const float& angleDegree);
//  constexpr float toDegree(const float& angleRadian);
  float toDegree(const float& angleRadian);

  inline double cot(const double& angleRadian);

  double cotDegree(const double& angleDegree);


  constexpr u_long pow(u_long base, u_long exponent) {
    if(exponent == 0)
      return 1;
    return base * pow(base, exponent -1);
  }
  constexpr u_short pow2(u_short exponent) {
    return pow(2,exponent);
  }
}