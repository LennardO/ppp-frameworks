//
// Created by lennard on 3/9/21.
//

#pragma once

#include <chrono>
#include <iostream>

std::ostream& operator<<(std::ostream& o, std::chrono::nanoseconds ns);

namespace tt {

  std::string bytesToReadableStr(u_long bytes);
}