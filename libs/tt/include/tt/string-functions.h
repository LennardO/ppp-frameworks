//
// Created by lennard on 3/12/21.
//

#pragma once

#include <vector>
#include <string>

namespace tt {

  std::vector<std::string> splitIntoWords(const std::string& str);
}