//
// Created by lennard on 3/10/21.
//

#include "tt.h"

namespace tt {

  float randomFloat01() {
    static std::uniform_real_distribution<float> unif(0.0,std::nextafter(1.0, std::numeric_limits<float>::max()));
    static std::random_device rd;
    static std::default_random_engine re(rd());
    return unif(re);
  }
}