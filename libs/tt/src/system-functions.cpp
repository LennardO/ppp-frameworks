//
// Created by lennard on 3/12/21.
//

#include "tt/system-functions.h"
#include "tt/file-functions.h"

namespace tt {

  ulong ownVSizeInBytes() {
    return std::stoul(readWordsFromFile("/proc/self/stat").at(22)); // https://man7.org/linux/man-pages/man5/proc.5.html
  }

  std::chrono::nanoseconds elapsedTime() {
    static auto t0 = std::chrono::steady_clock::now();
    return std::chrono::steady_clock::now() - t0;
  }
}