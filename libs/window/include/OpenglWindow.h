//
// Created by lennard on 10/14/20.
//
#pragma once

#include <iostream>
#include <GL/glew.h> // should be included before GLFW
#include <GLFW/glfw3.h>

class OpenglWindow {
public:
  OpenglWindow();

  void create();
  [[nodiscard]] uint width() const;
  [[nodiscard]] uint height() const;
  /// @brief returns width/height
  [[nodiscard]] double aspectRatio() const;
  bool shouldClose();
  void finalizeFrame();
  void close();

private:
  GLFWwindow* _window = nullptr;
  uint _width, _height;
};