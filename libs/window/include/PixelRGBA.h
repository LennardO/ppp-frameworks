//
// Created by lennard on 10/21/20.
//

#pragma once

#include <sys/types.h>

struct PixelRGBA { // todo: aka Quadruple/Quad
  u_char r;
  u_char g;
  u_char b;
  u_char a;
};