#!/usr/bin/env python3

import subprocess
import os
from argparse import ArgumentParser

def docker_run_gpu(imageName, command):
    fullCommand = "docker run --rm --cap-add=sys_ptrace -eDISPLAY=" + os.environ["DISPLAY"] + " --volume=/tmp/.X11-unix:/tmp/.X11-unix --device=/dev/dri --privileged --name=tmp_gpu --volume=" + os.getcwd() + ":/hostPWD --workdir=/hostPWD " + imageName + " " + command
    print("running:\n" + fullCommand)
    subprocess.run(fullCommand, shell=True, check=True)

def multipleCommands(args):
    result = "bash -c \""
    result += " && ".join(args)
    result += "\""
    return result

def runExampleCommands(backend, use_gpu, example):
    targetStr = "gpu" if use_gpu else "cpu"
    useGPU_OnOFF = "ON" if use_gpu else "OFF"
    buildDir = "docker-builds/" + backend + "-" + targetStr
    commands = ["mkdir -p " + buildDir, "cd " + buildDir]
    hipsyclWorkaround = ""
    if backend == "sycl":
        hipsyclPlatform = "cuda" if use_gpu else "cpu"
        hipsyclWorkaround = " -D HIPSYCL_PLATFORM=" + hipsyclPlatform
        if use_gpu:
            hipsyclWorkaround = hipsyclWorkaround + " -D HIPSYCL_GPU_ARCH=sm_52"
    commands.append("cmake -D USE_" + backend.upper() + "=ON -D DEFAULT_GPU=" + useGPU_OnOFF + hipsyclWorkaround + "-D CMAKE_BUILD_TYPE=Release ../..")
    commands.append("make -j $(nproc) --no-print-directory " + example)
    commands.append("./examples/" + example + "/" + example)
    return commands

def runExample(backend, use_gpu, example, image=None):
    if image is None:
        image = backend
    docker_run_gpu(image, multipleCommands(runExampleCommands(backend, use_gpu, example)))

parser = ArgumentParser()
parser.add_argument("--clean", help="delete all build directories", action="store_true")
args = parser.parse_args()

if args.clean:
    subprocess.run(["sudo rm -rf docker-builds"], shell=True, check=True)

# subprocess.run("mkdir -p build-cpu && cd build-cpu && cmake -D USE_OPENMP=ON -D DEFAULT_GPU=OFF -D CMAKE_BUILD_TYPE=Release .. && make -j $(nproc) --no-print-directory SaxpyUnifiedPP && ./examples/SaxpyUnifiedPP/SaxpyUnifiedPP", shell=True, check=True)
# subprocess.run("mkdir -p build-gpu && cd build-gpu && cmake -D DEFAULT_GPU=ON  -DCMAKE_BUILD_TYPE=Release .. && make -j $(nproc) --no-print-directory SaxpyUnifiedPP && ./examples/SaxpyUnifiedPP/SaxpyUnifiedPP", shell=True, check=True)
#
# runExample(backend="kokkos", use_gpu=False, example="SaxpyUnifiedPP")
runExample(backend="kokkos", use_gpu=True, example="SaxpyUnifiedPP")

# runExample(backend="raja", use_gpu=False, example="SaxpyUnifiedPP")
# runExample(backend="raja", use_gpu=True, example="SaxpyUnifiedPP")
#
# runExample(backend="sycl", use_gpu=False, example="SaxpyUnifiedPP")
# # runExample(backend="sycl", use_gpu=True, example="SaxpyUnifiedPP")

# runExample(backend="openmp-target", use_gpu=False, example="SaxpyUnifiedPP")
# runExample(backend="openmp-target", use_gpu=True, example="SaxpyUnifiedPP")
# GPU too old:
# nvc++-Error-OpenMP GPU Offload is available only on systems with NVIDIA GPUs with compute capability '>= cc70'
# so at least a RTX2060 or TITAN V is needed