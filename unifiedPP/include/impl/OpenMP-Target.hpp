
#pragma once
#include <iostream>

#define UPP_LAMBDA [=]

namespace uPP {

  using index = size_t;
  using range = size_t;

  struct DataUsage {
    bool input;
    bool output;
  };
  const DataUsage Input = {true, false};
  const DataUsage Output = {false, true};
  const DataUsage InputOutput = {true, true};

  template<typename F>
  void forall(int N, F f) {
//    #pragma omp target
    #pragma omp parallel for
    for(int i=0; i<N; i++)
      f(i);
  }

  template<typename T>
  class ArrayHandler {
  public:
    using Value = T;
    using View = T*;
    using ConstView = const T*;

    ArrayHandler(const std::vector<T>& data_host, DataUsage usage=Input)
    : size_{data_host.size()}
    , data_host_const_{data_host.data()}
    {}
    ArrayHandler(std::vector<T>& data_host, DataUsage usage=InputOutput)
    : size_{data_host.size()}
    , data_host_const_{data_host.data()}
    , data_host_{data_host.data()}
    {
//      if(usage.output)
//        enableCopyBack();
    }

//    ~ArrayHandler() {
//      if(constAccess_) {
//        #pragma omp target exit data map(delete: data_host_const_[0, size_])
//      }
//      if(writeAccess_) {
//        if(copyBack_) {
//          #pragma omp target exit data map(from: data_host_[0,size_])
//        }
//        else {
//          #pragma omp target exit data map(delete: data_host_[0, size_])
//        }
//      }
//    }

    size_t size() const {
      return size_;
    }

    bool constInitialized() const {
      return data_host_ == nullptr;
    }

    ConstView readView() {
//      if(!constAccess_)
//        transferConstToDevice();
      return data_host_const_;
    }

    View writeView() {
//      if(constInitialized())
//        throw std::runtime_error("The OpenMP-Target backend does not support write access for arrays initialized with const data. Please use getConstDeviceAccess or another backend.");
//      if(!writeAccess_)
//        transferToDevice();
      return data_host_;
    }

    View readWriteView() {
//      if(constInitialized())
//        throw std::runtime_error("The OpenMP-Target backend does not support write access for arrays initialized with const data. Please use getConstDeviceAccess or another backend.");
//      if(!writeAccess_)
//        transferToDevice();
      return data_host_;
    }

  private:
    void transferToDevice() {
//      writeAccess_ = true;
//      #pragma omp target enter data map(to: data_host_[0,size_])
    }
    void transferConstToDevice() {
//      constAccess_ = true;
//      #pragma omp target enter data map(to: data_host_const_[0,size_])
    }

    void enableCopyBack() {
//      if(constInitialized())
//        throw std::runtime_error("can't transfer back from device when the array was initialized with const data");
//      copyBack_ = true;
    }

  private:
    size_t size_;
    T* data_host_ = nullptr;
    const T* data_host_const_ = nullptr;
//    bool copyBack_ = false;
//    bool writeAccess_ = false;
//    bool constAccess_ = false;
  };

}