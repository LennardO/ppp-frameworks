
#pragma once

#include <cf/ArrayHandler.hpp>
#include <cf/forall.hpp>

#define UPP_LAMBDA CF_LAMBDA

namespace uPP = cf;

namespace cf {
  using range = size_t;
  using index = size_t;
}
