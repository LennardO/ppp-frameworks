//
// Created by lennard on 3/1/21.
//

#pragma once
#include <RAJA/RAJA.hpp>
#include <tt/prettyStream.h>
#include <atomic>

//#define UPP_LAMBDA [=] RAJA_DEVICE
#define UPP_LAMBDA [=]


namespace uPP {

  using range = size_t;
  using index = size_t;

  struct DataUsage {
    bool input;
    bool output;
  };
  const DataUsage Input = {true, false};
  const DataUsage Output = {false, true};
  const DataUsage InputOutput = {true, true};

  class Device {
  public:
    #if CF_DEFAULT_TARGET_GPU
      using RajaPolicy = RAJA::cuda_exec<256>;
      inline static const std::string name = "Cuda";
    #else
      using RajaPolicy = RAJA::omp_parallel_for_exec;
      inline static const std::string name = "OpenMP";
    #endif
  };

  template<typename F>
  void forall(int N, F f) {
    auto t0 = std::chrono::steady_clock::now();
    RAJA::forall<Device::RajaPolicy>(RAJA::RangeSegment(0,N), f);
    auto t1 = std::chrono::steady_clock::now();
    std::cout << "RAJA::forall took " << t1-t0 << '\n';
  }

  using ArrayHandlerID = int;
  static std::atomic <ArrayHandlerID> ah_id_counter;

  template<typename T>
  class ArrayHandler {
  public:
    using Value = T;
    using View = T*;
    using ConstView = const T*;

    ArrayHandler(const std::vector<T>& data_host, DataUsage usage=Input)
    : data_host_const_(data_host.data())//todo: auch data_dvc_ setzen
    , size_(data_host.size())
    {
      #if CF_DEFAULT_TARGET_GPU
        auto t0 = std::chrono::steady_clock::now();
        cudaErrchk( cudaMalloc(&data_dvc_, sizeof(T) * size_) );
        auto t1 = std::chrono::steady_clock::now();
        std::cout << "AH " << id() << ": allocating on Cuda took " << t1-t0 << '\n';
        data_dvc_const_ = data_dvc_;
      #else
        data_dvc_const_ = data_host_const_;
      #endif
      if(usage.input)
        transferToDevice();
    }
    ArrayHandler(std::vector<T>& data_host, DataUsage usage=InputOutput)
    : ArrayHandler((const std::vector<T>&)data_host, usage)
    {
      data_host_ = data_host.data();
      #if !CF_DEFAULT_TARGET_GPU
        data_dvc_ = data_host_;
      #endif
      if(usage.output)
        enableCopyBack();
    }

    ~ArrayHandler() {
      #if CF_DEFAULT_TARGET_GPU
        if(copyBack_) {
          auto t0 = std::chrono::steady_clock::now();
          cudaErrchk( cudaMemcpy(data_host_, data_dvc_, sizeof(T) * size_, cudaMemcpyDeviceToHost) );
          auto t1 = std::chrono::steady_clock::now();
          std::cout << "AH " << id() <<" transfering data from Cuda to host_external took " << t1-t0 << '\n';
        }
        cudaErrchk( cudaFree(data_dvc_) );
      #endif
    }

    [[nodiscard]]
    auto id() const -> ArrayHandlerID {
      return id_;
    }

    [[nodiscard]] size_t size() const {
      return size_;
    }

    [[nodiscard]] bool constInitialized() const {
      return data_host_ == nullptr;
    }

    ConstView readView() {
      return data_dvc_const_;
    }

    View writeView() {
      if(constInitialized())
        throw std::runtime_error("write access to const initialized array is not implemented yet");
      return data_dvc_;
    }

    View readWriteView() {
      if(constInitialized())
        throw std::runtime_error("write access to const initialized array is not implemented yet");
      return data_dvc_;
    }

  private:
    void transferToDevice() {
      #if CF_DEFAULT_TARGET_GPU
        auto t0 = std::chrono::steady_clock::now();
        cudaErrchk( cudaMemcpy(data_dvc_, data_host_const_, sizeof(T) * size_, cudaMemcpyHostToDevice) );
        auto t1 = std::chrono::steady_clock::now();
        std::cout << "AH " << id() <<" transfering data from host_external to Cuda took " << t1-t0 << '\n';
      #endif
    }

    void enableCopyBack() {
      if(constInitialized())
        throw std::runtime_error("can't transfer back from device when the array was initialized with const data");
      copyBack_ = true;
    }

  private:
    ArrayHandlerID id_ = ah_id_counter++;
    T* data_host_ = nullptr;
    const T* data_host_const_ = nullptr;
    size_t size_;
    bool copyBack_ = false;
    View data_dvc_ = nullptr;
    ConstView data_dvc_const_;
  };


}