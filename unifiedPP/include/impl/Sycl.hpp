//
// Created by lennard on 3/7/21.
//

#pragma once
#include <CL/sycl.hpp>
#include <tt/prettyStream.h>

#define UPP_LAMBDA [=]

namespace uPP {

  using range = cl::sycl::range<1>;
  using index = cl::sycl::id<1>;

  struct DataUsage {
    bool input;
    bool output;
  };
  const DataUsage Input = {true, false};
  const DataUsage Output = {false, true};
  const DataUsage InputOutput = {true, true};

  class Context {
  public:
    Context() {
      std::cout << "initialized SYCL backend" << '\n';
    }

    template<typename F>
    void runOnSubmit(F f) {
      _functors.push_back(f);
    }

    template<typename F>
    void forall(range N, F f) {
      cl::sycl::queue q;
      q.submit([&](cl::sycl::handler& cgh) {
        for (auto& functor : _functors)
          functor(cgh);
        auto t0 = std::chrono::steady_clock::now();
        cgh.parallel_for<class forall>(N, f);
        auto t1 = std::chrono::steady_clock::now();
        std::cout << "SYCL cgh.parallel_for took " << t1-t0 << '\n';
      });
      _functors.clear();
    }

  private:
    std::vector <std::function<void(cl::sycl::handler & cgh)>> _functors;
  };
  std::shared_ptr<Context> getContext() {
    static std::shared_ptr<Context> ctx_ptr = std::make_shared<Context>();
    return ctx_ptr;
  }

  template<typename F>
  void forall(range N, F f) {
    getContext()->forall(N,f);
  }

  using ArrayHandlerID = int;
  static std::atomic <ArrayHandlerID> ah_id_counter;

  template<typename T>
  class ArrayHandler : public cl::sycl::buffer<T> {
  public:
    using Parent = cl::sycl::buffer<T>;
    using Value = T;
    using View = cl::sycl::accessor<T, 1, cl::sycl::access::mode::read_write, cl::sycl::access::target::global_buffer, cl::sycl::access::placeholder::true_t>;
    using ConstView = cl::sycl::accessor<T, 1, cl::sycl::access::mode::read, cl::sycl::access::target::global_buffer, cl::sycl::access::placeholder::true_t>;

    ArrayHandler(const std::vector <T>& data_host, DataUsage usage=Input)
    : Parent(data_host.data(), data_host.size()) {}

    ArrayHandler(std::vector <T>& data_host, DataUsage usage=InputOutput)
    : Parent(data_host.data(), data_host.size()) {}

    [[nodiscard]]
    auto id() const -> ArrayHandlerID {
      return id_;
    }

    size_t size() const {
      return Parent::get_count();
    }

    View readView() {
      //todo: prevent copy back
      cl::sycl::accessor<T, 1, cl::sycl::access::mode::read_write, cl::sycl::access::target::global_buffer, cl::sycl::access::placeholder::true_t> acc{
        *this};
      getContext()->runOnSubmit([=](cl::sycl::handler& cgh) {
        cgh.require(acc);
      });
      return acc;
    }

    View writeView() {
      //todo: use accessor property::no_init to prevent copy to device
      cl::sycl::accessor<T, 1, cl::sycl::access::mode::read_write, cl::sycl::access::target::global_buffer, cl::sycl::access::placeholder::true_t> acc{
        *this};
      getContext()->runOnSubmit([=](cl::sycl::handler& cgh) {
        cgh.require(acc);
      });
      return acc;
    }

    View readWriteView() {
      cl::sycl::accessor<T, 1, cl::sycl::access::mode::read_write, cl::sycl::access::target::global_buffer, cl::sycl::access::placeholder::true_t> acc{
        *this};
      getContext()->runOnSubmit([=](cl::sycl::handler& cgh) {
        cgh.require(acc);
      });
      return acc;
    }

  private:
    ArrayHandlerID id_ = ah_id_counter++;
  };
}
